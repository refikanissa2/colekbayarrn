import React, { Component } from 'react';
import { PermissionsAndroid, Alert, AsyncStorage } from 'react-native';
import { Provider, connect } from 'react-redux'
import { Actions, Router, Scene } from 'react-native-router-flux';

// import { store } from './src/config/store'
import { Colors } from './assets/styles/Colors'
import { saveTracking, getCurrentPosition } from './src/actions/globalAction'

import HeaderImage from './src/components/HeaderImage'
import NavBarCustom from './src/components/NavBarCustom'
import LoginScreen from './src/screens/LoginScreen'
import TaskScreen from './src/screens/TaskScreen'
import ActivityScreen from './src/screens/ActivityScreen'
import ReportScreen from './src/screens/ReportScreen'
import DetailTaskScreen from './src/screens/DetailTaskScreen'
import SubmitDetailTask from './src/screens/SubmitDetailTask'
import PaymentConfirm from './src/screens/PaymentConfirm'
import SettleScreen from './src/screens/SettleScreen'

import configureStore from "./src/config/configureStore";
const store = configureStore();

const ConnectedRouter = connect()(Router);

const Scenes = Actions.create(
  <Scene key="root">
    <Scene key="login" component={LoginScreen} type={Actions.REPLACE} hideNavBar initial/>
    <Scene key="home" title="Home"
      showLabel={true}
      tabs={true}
      type={Actions.REPLACE}
      navBar={HeaderImage}
      tabBarStyle={{backgroundColor: Colors.COLEKBAYAR, alignItems: 'center', justifyContent: 'center', paddingBottom: 15}}
      labelStyle={{fontSize: 16, alignItems: 'center', justifyContent: 'center'}}
      activeTintColor={"white"}
      inactiveTintColor={Colors.LIGHTGREY}
    >
      <Scene key="task" title="Task List" tabBarLabel="List To Do" component={TaskScreen} hideNavBar/>
      <Scene key="activity" title="Activity" tabBarLabel="Activity" component={ActivityScreen} hideNavBar/>
      <Scene key="report" title="Report" tabBarLabel="Report" component={ReportScreen} hideNavBar/>
    </Scene>
    <Scene key="detail" title="Task Detail" component={DetailTaskScreen} navBar={NavBarCustom}/>
    <Scene key="settle" title="Settle List" component={SettleScreen} hideNavBar/>
    <Scene key="submitDetail" title="Submit Task" component={SubmitDetailTask} navBar={NavBarCustom}/>
    <Scene key="paymentConfirm" title="Payment Confirm" component={PaymentConfirm} navBar={NavBarCustom}/>
  </Scene>
)

export default class App extends Component<{}> {
  componentDidMount(){
    AsyncStorage.getItem("grantedLocation", (err, result) => {
      if(result == null){
        this._requestForPermission();
      }else if(result == "granted"){
        this.getLocation();
        store.dispatch(getCurrentPosition());
      }else{
        this._requestForPermission();
      }
    })
  }

  getLocation(){
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        var date = new Date().getDate();
        var month = new Date().getMonth();
        var year = new Date().getFullYear();
        var hour = new Date().getHours();
        var minute = new Date().getMinutes();
        var second = new Date().getSeconds();
        var fullDate = date+"/"+(month+1)+"/"+year+" "+("0" + hour).slice(-2)+":"+("0" + minute).slice(-2)+":"+("0" + second).slice(-2);
        var tmpPosition = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          timeTracking: fullDate
        }
        AsyncStorage.getItem("dataPosition", (err, result) => {
          if(result == null){
            var dataPosition = [];
            dataPosition.push(tmpPosition);
            AsyncStorage.setItem("dataPosition", JSON.stringify(dataPosition));
          }else{
            var dataPosition = JSON.parse(result);
            dataPosition.push(tmpPosition);
            console.log(" isi ", dataPosition);
            AsyncStorage.setItem("dataPosition", JSON.stringify(dataPosition));
            if(dataPosition.length >= 10 && store.getState().globalReducer.dataUser != undefined){
              AsyncStorage.getItem("jwttoken", (err, result) => {
                store.dispatch(saveTracking(result, dataPosition));
              })
            }
          }
        })
      },
      (error) => console.log("error ", error),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 50 }
    );
  }

  async _requestForPermission(){
    try{
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
      ])
      if(granted["android.permission.ACCESS_FINE_LOCATION"] == "granted"){
        let grantedLoc = "granted";
        AsyncStorage.setItem("grantedLocation", grantedLoc);
        this.getLocation();
        store.dispatch(getCurrentPosition());
      }
      return true
    }catch(err){
      Alert.alert("Error Permission", err)
    }
  }

  componentWillUnmount(){
    navigator.geolocation.clearWatch(this.watchId);
  }

  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter scenes={Scenes}/>
      </Provider>
    );
  }
}
