import React, { Component } from 'react'
import { AppRegistry, View, StatusBar, AsyncStorage } from 'react-native';
import App from './App';

import { Colors } from './assets/styles/Colors'

export default class ColekBayarPhase2 extends Component {
  render(){
    return(
      <View style={{flex: 1}}>
        <StatusBar backgroundColor={Colors.COLEKBAYAR}/>
        <App/>
      </View>
    )
  }
}

AppRegistry.registerComponent('ColekBayarPhase2', () => ColekBayarPhase2);
