import { AsyncStorage, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import API from '../data/API'

export function getActivityList(jwttoken){
    return (dispatch, getState) => {
        const { tokenId } = getState().globalReducer;
        return API.activityList(jwttoken, tokenId)
        .then((response) => {
            if(response.status == 200){
                response.json().then((responseJson) => {
                    if(responseJson.result == "Y"){
                        console.log("response ", responseJson)
                    }else{
                        Alert.alert(responseJson.code, responseJson.message);
                    }
                })
            }else if(response.status == 401){
                response.json().then((responseJson) => {
                    Alert.alert("Error 401", responseJson.message);
                    AsyncStorage.removeItem("storagePin");
                    Actions.login();
                })
            }else{
                response.json().then((responseJson) => {
                    Alert.alert("Error 401", responseJson.message);
                    AsyncStorage.removeItem("storagePin");
                    Actions.login();
                })
            }
        })
    }
}