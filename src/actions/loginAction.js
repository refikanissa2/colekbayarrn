import { NativeModules, Alert, AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import DeviceInfo from 'react-native-device-info'

import API from '../data/API'
import { setJWTToken, setLoading } from './globalAction'

export const SET_DEVICE_INFO = 'SET_DEVICE_INFO'
export const SET_PHONE_NO = 'SET_PHONE_NO'

function setDeviceInfo(deviceInfo) {
  return {
    type: SET_DEVICE_INFO,
    deviceInfo
  };
}

function setPhoneNo(phoneNo) {
  return {
    type: SET_PHONE_NO,
    phoneNo
  };
}

export function getDeviceInfo(fromLogin, phoneNo) {
  return dispatch => {
    let IMEI, IMSI, deviceInfo, deviceId;
    NativeModules.IMEI.getIMEI().then(result=>{
        if (result && result.length >0) {
            IMEI = result;
            NativeModules.IMEI.getIMSI().then(result => {
              if(result && result.length > 0){
                IMSI = result;
                deviceId = DeviceInfo.getDeviceId();
                deviceInfo = {
                  IMEI: IMEI,
                  IMSI: IMSI,
                  deviceId: deviceId
                }
                dispatch(setDeviceInfo(deviceInfo));
                if(fromLogin){
                  dispatch(submitLogin(IMSI, IMEI, deviceId, phoneNo));
                }
              }else{
                console.log('Error get IMSI', result);
              }
            })
        }else{
          console.log('Error get IMEI', result);
        }
    });
  }
}

export function submitLogin(imsi, imei, deviceId, phoneNo){
  return dispatch => {
    if(imsi == null){
      dispatch(getDeviceInfo(true, phoneNo));
    }else{
      dispatch(setLoading(true));
      return API.loginApi(imsi, imei, deviceId, phoneNo)
        .then((response) => {
          if(response.status == 200){
            const jwttoken = response.headers.get('token');
            response.json().then((responseJson) => {
              if(responseJson.result == "Y"){
                AsyncStorage.setItem("jwttoken", jwttoken);
                dispatch(setPhoneNo(phoneNo));
                if(responseJson.data.activation == "Y"){
                  Actions.refresh({statusLogin: 'otp'})
                }else{
                  Actions.refresh({statusLogin: 'createPin'})
                }
              }else{
                dispatch(setPhoneNo(null));
                Alert.alert("Error", "Connection Failed!");
                Actions.refresh({statusLogin: 'mobilePhone'});
              }
            })
          }else if (response.status == 401) {
            response.json().then((responseJson) => {
              Alert.alert("Error 401", responseJson.message);
              AsyncStorage.removeItem("storagePin");
              Actions.login();
            })
          }else {
            response.json().then((responseJson) => {
              Alert.alert("Error Net", responseJson.message);
              AsyncStorage.removeItem("storagePin");
              Actions.login()
            })
          }
          dispatch(setLoading(false));
        })
        .catch((error) => {
          console.log('Error fetch ', error);
          throw error;
        });
    }
  }
}

export function activate(phoneNo, tokenotp, jwttoken) {
  return dispatch => {
    dispatch(setLoading(true))
    return API.activate(phoneNo, tokenotp, jwttoken)
    .then((response) => {
      if(response.status === 200){
        response.json().then((responseJson) => {
          if (responseJson.result === "Y") {
            Alert.alert("Success", responseJson.message);
            Actions.refresh({statusLogin: 'createPin'});
          }else{
            Actions.refresh({statusLogin: 'otp'});
            Alert.alert("Error", responseJson.message);
          }
        })
      }
      else{
        Alert.alert("Error Network", response);
        Actions.refresh({statusLogin: 'mobilePhone'})
      }
      dispatch(setLoading(false));
    })
    .catch((error) => {
      console.log('Error fetch ', error);
      throw error;
    });
  };
}
