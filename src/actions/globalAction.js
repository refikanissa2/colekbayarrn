import { AsyncStorage, Alert, NativeModules } from 'react-native'

import API from '../data/API'

export const SET_TOKEN_ID = 'SET_TOKEN_ID'
export const SET_LOADING = 'SET_LOADING'
export const SET_DATA_USER = 'SET_DATA_USER'
export const SET_PROGRESS_BAR = 'SET_PROGRESS_BAR'
export const SET_CURRENT_POSITION = 'SET_CURRENT_POSITION'

export function setTokenId(tokenId) {
  return {
    type: SET_TOKEN_ID,
    tokenId
  };
}

export function setLoading(isLoading) {
  return {
    type: SET_LOADING,
    isLoading
  };
}

function setCurrentPosition(position){
  return{
    type: SET_CURRENT_POSITION,
    position
  }
}

export function getCurrentPosition(){
  return dispatch => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let newData = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
        dispatch(setCurrentPosition(newData));
      },
      (error) => {
        dispatch(getCurrentPosition())
      },
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    )
  }
}

export function getRefNumber(username){
  return dispatch => {
    const date = new Date();
    const currentTime = date.getTime();
    const deviceRefNo = username + (currentTime + 1);
    return deviceRefNo;
  }
}

export function getTokenKey(invoiceNumber, refNumber, installment, lat, lng){
  return dispatch => {
    const combine = invoiceNumber + "PAY coLek B4y4r" + installment + lat.toString() + lng.toString() + refNumber
    let tokenKey;
    NativeModules.IMEI.genTokenKey(combine).then(result => {
      tokenKey = result;
      console.log('result ', result);
    })
    console.log('res token ', tokenKey);
    return tokenKey;
  }
}

export function setDataUser(dataUser) {
  return {
    type: SET_DATA_USER,
    dataUser
  };
}

export function setProgressBar(progressBar){
  return {
    type: SET_PROGRESS_BAR,
    progressBar
  }
}

export function saveTracking(jwttoken, dataPosition) {
  return (dispatch, getState) => {
    const { tokenId, dataUser } = getState().globalReducer;

    let lat = [];
    let lng = [];
    let timeTracking = [];
    dataPosition.map((data, key) => {
      lat.push(data.lat.toString());
      lng.push(data.lng.toString());
      timeTracking.push(data.timeTracking);
    })

    const data = new FormData();
    data.append("tokenId", tokenId);
    data.append("lat", JSON.stringify(lat));
    data.append("lng", JSON.stringify(lng));
    data.append("timeTracking", JSON.stringify(timeTracking));
    data.append("nik", dataUser.userName);

    const headers = new Headers();
    headers.append('jwttoken', jwttoken);

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }

    return API.saveTracking(config)
    .then((response) => {
      if(response.status == 200){
        response.json().then((responseJson) => {
          if(responseJson.result != "Y"){
            Alert.alert(responseJson.code, responseJson.message);
          }else{
            console.log("success save tracking")
            AsyncStorage.removeItem("dataPosition");
          }
        })
      } else if (response.status == 401) {
        response.json().then((responseJson) => {
          Alert.alert("Error 401", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login();
        })
      } else {
        response.json().then((responseJson) => {
          Alert.alert("Error Net", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login()
        })
      }
    })
    .catch((error) => {
      console.log('Error fetch ', error);
      throw error;
    });
  }
}
