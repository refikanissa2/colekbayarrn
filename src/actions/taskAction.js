import {Alert, AsyncStorage, NativeModules} from 'react-native'
import {Actions} from 'react-native-router-flux'
var ImagePicker = require('react-native-image-picker');
import ImageMarker from "react-native-image-marker"

import { setLoading, setDataUser, getRefNumber, setProgressBar } from './globalAction'
import API from '../data/API'

export const SET_TRX_LIST = 'SET_TRX_LIST'
export const SET_REFRESH_LIST = 'SET_REFRESH_LIST'
export const SET_ALL_REASON = 'SET_ALL_REASON'
export const SET_PAYMENT_TYPE = 'SET_PAYMENT_TYPE'
export const SET_SURVEY_LIST = 'SET_SURVEY_LIST'
export const SET_BANK_LIST = 'SET_BANK_LIST'
export const SET_IMAGE_TYPE = 'SET_IMAGE_TYPE'
export const SET_LIST_PREDEFINE = 'SET_LIST_PREDEFINE'
export const SET_TASK_MEDIA = 'SET_TASK_MEDIA'
export const SET_MESSAGE_LOAD = 'SET_MESSAGE_LOAD'
export const SET_SETTLE_LIST = "SET_SETTLE_LIST"

function setTrxList(trxList) {
  return {
    type: SET_TRX_LIST,
    trxList
  };
}

function setRefreshList(isRefreshing) {
  return {
    type: SET_REFRESH_LIST,
    isRefreshing
  };
}

function setAllReason(allReason) {
  return {
    type: SET_ALL_REASON,
    allReason
  };
}

function setPaymentType(paymentType) {
  return {
    type: SET_PAYMENT_TYPE,
    paymentType
  };
}

function setSurveyList(surveyList) {
  return {
    type: SET_SURVEY_LIST,
    surveyList
  };
}

function setBankList(bankList) {
  return {
    type: SET_BANK_LIST,
    bankList
  };
}

function setImageType(imageType) {
  return {
    type: SET_IMAGE_TYPE,
    imageType
  }
}

function setListPredefine(listPredefine) {
  return {
    type: SET_LIST_PREDEFINE,
    listPredefine
  };
}

export function setTaskMedia(taskMedia){
  return {
    type: SET_TASK_MEDIA,
    taskMedia
  }
}

export function openCamera(imageType) {
  return (dispatch, getState) => {
    const { taskMedia } = getState().taskReducer;
    const { currentPosition } = getState().globalReducer;
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true,
        waitUntilSaved: true
      },
      maxWidth: 1000,
      maxHeight: 1000
    };

    let lat = null;
    let lng = null;
    var date = new Date().getDate();
    var month = new Date().getMonth();
    var year = new Date().getFullYear();
    var hour = new Date().getHours();
    var minute = new Date().getMinutes();
    var second = new Date().getSeconds();
    let fullDate = date+"/"+(month+1)+"/"+year+" "+("0" + hour).slice(-2)+":"+("0" + minute).slice(-2)+":"+("0" + second).slice(-2);

    dispatch(setLoading(true));

    ImagePicker.launchCamera(options, (response)  => {
      const encoded = `jpg,${response.data}`;
      const uri = response.uri;
      console.log("imgpicekr ", response)
      let tmpTaskMedia = taskMedia;
          
      const textMarker = "Lat : "+currentPosition.lat+"\n" +
      "Lng : "+currentPosition.lng+"\n" +
      "Date : "+fullDate+"";
      ImageMarker.addText(
        response.path, 
        textMarker, 
        15, 
        70,
        '#FAFAFA', 
        'Arial-BoldItalicMT', 
        20
      ).then((res) => {
        let uriImageMarker = 'file://'+res;
        NativeModules.RNImageToBase64.getBase64String(uriImageMarker, (err, base64) => {
          let encodedMarker = `jpg,${base64}`;
          tmpTaskMedia.push({
            imageType: imageType,
            imgUri: uriImageMarker,
            imgBase64: encodedMarker
          });
          dispatch(setTaskMedia(tmpTaskMedia));
          dispatch(setLoading(false));
          Actions.submitDetail({type: 'refresh'})
        })
      }).catch((err) => {
          console.log(err)
      })
    });
  }
}

function setMessageLoad(message){
  return {
    type: SET_MESSAGE_LOAD,
    message
  }
}

export function setSettleList(dataSettle){
  return {
    type: SET_SETTLE_LIST,
    dataSettle
  }
}

export function getTaskList(jwttoken, tokenId, isRefreshControl) {
  return dispatch => {
    if(isRefreshControl == 1){
      dispatch(setRefreshList(true));
    }else{
      dispatch(setLoading(true));
    }
    return API.taskList(jwttoken, tokenId)
    .then((response) => {
      if (response.status == 200) {
        response.json().then((responseJson) => {
          console.log("all ", responseJson);
          if (responseJson.result == "Y") {
            var newData = {
              agentName: responseJson.agentName,
              corpName: responseJson.corpName,
              emailAgent: responseJson.emailAgent,
              mobileNoAgent: responseJson.mobileNoAgent,
              userName: responseJson.userName
            }
            dispatch(setDataUser(newData));
            dispatch(setTrxList(responseJson.trxList));
            dispatch(setAllReason(responseJson.reasonList));
            dispatch(setPaymentType(responseJson.paymentType));
            dispatch(setSurveyList(responseJson.srvyList));
            dispatch(setBankList(responseJson.bankList));
            dispatch(setImageType(responseJson.imageType));
            dispatch(setListPredefine(responseJson.listPredefine));
          } else {
            Alert.alert(responseJson.code, responseJson.message)
          }
        })
      } else if (response.status == 401) {
        response.json().then((responseJson) => {
          Alert.alert("Error 401", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login();
        })
      } else {
        response.json().then((responseJson) => {
          Actions.settle({"jwttoken": jwttoken, "tokenId": tokenId})
        })
      }
      if(isRefreshControl == 1){
        dispatch(setRefreshList(false));
      }else{
        dispatch(setLoading(false));
      }
    })
    .catch((error) => {
      console.log('Error fetch ', error);
      throw error;
    });
  };
}

export function getSettleList(jwttoken, tokenId){
  return dispatch => {
    return API.settleList(jwttoken, tokenId)
    .then((response) => {
      if (response.status == 200) {
        response.json().then((responseJson) => {
          let settleChecked = [];
          for(var i=0; i<responseJson.trxList.length; i++){
            settleChecked.push(false);
          }
          let tmpSettleList = {
            totalSettlement: responseJson.totalSettlement,
            date: new Date().toLocaleString("id"),
            trxList: responseJson.trxList,
            settleChecked: settleChecked
          }
          dispatch(setSettleList(tmpSettleList));
        })
      }else if (response.status == 401) {
        response.json().then((responseJson) => {
          Alert.alert("Error 401", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login();
        })
      }else {
        response.json().then((responseJson) => {
          Alert.alert("Error Net", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login()
        })
      }
    })
    .catch((error) => {
      console.log('Error fetch ', error);
      throw error;
    });
  }
}

export function submitSettle(jwttoken, tokenId, refNo, last){
  return (dispatch, getState) => {
    return API.submitSettle(jwttoken, tokenId, refNo)
    .then((response) => {
      if(response.status == 200){
        response.json().then((responseJson) => {
          if(responseJson.result == "Y"){
            if(last){
              Actions.home()
            }
          }else{
            Alert.alert(responseJson.code, responseJson.message);
          }
        })
      }else if (response.status == 401) {
        response.json().then((responseJson) => {
          Alert.alert("Error 401", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login();
        })
      }else {
        response.json().then((responseJson) => {
          Alert.alert("Error Net", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login()
        })
      }
    })
    .catch((error) => {
      console.log("Error fetch ", error)
      throw error;
    })
  }
}

export function submitTaskPay(itemToSubmit, jwttoken, tokenId, dataUser, isVisited) {
  return (dispatch , getState) => {
    const { currentPosition } = getState().globalReducer;
    let progressBar = {
      isShow: true,
      progress: 0
    };
    dispatch(setProgressBar(progressBar))
    dispatch(setMessageLoad("Uploading Task"));
    const deviceRefNo = dispatch(getRefNumber(dataUser.userName));
    const lat = currentPosition.lat;
    const lng = currentPosition.lng;
    const combineForToken = itemToSubmit.invoiceNumber + "PAY coLek B4y4r" + itemToSubmit.installment + lat.toString() + lng.toString() + deviceRefNo;

    NativeModules.IMEI.genTokenKey(combineForToken).then(result => {
      const data = new FormData();
      data.append("lat", lat.toString());
      data.append("lng", lng.toString());
      data.append("deviceRefNo", deviceRefNo);
      data.append("tokenId", tokenId);
      data.append("invoiceNo", itemToSubmit.invoiceNumber);
      data.append("installment", itemToSubmit.installment.toString());
      data.append("onlineFlag", "1");
      data.append("tokenKey", result);
      data.append("distributionId", itemToSubmit.distributionId.toString());
      data.append("trxDateMobile", itemToSubmit.trxDateMobile);
      data.append("activityId", itemToSubmit.activityId.toString());
      
      if(isVisited){
        data.append("reasonStatus", itemToSubmit.reasonStatus.toString());
        data.append("reasonVisit", itemToSubmit.reasonVisit.toString());
        data.append("reasonAttitude", itemToSubmit.reasonAttitude.toString());
        data.append("reasonDebitur", itemToSubmit.reasonDebitur.toString());
        data.append("reasonIsCooperative", itemToSubmit.reasonIsCooperative.toString());

        if(itemToSubmit.isPaid == "P"){
          itemToSubmit.paymentType.map((item, key) => {
            data.append("paymentType", JSON.stringify(item));
          })
          if(parseInt(itemToSubmit.installment) < parseInt(itemToSubmit.invoiceAmount)){
            data.append("reason", itemToSubmit.isPartial.reason.toString());
            data.append("nextCollectionDate", itemToSubmit.isPartial.nextCollectionDate);
            data.append("information", itemToSubmit.isPartial.information);
          }
        }else{
          data.append("reason", itemToSubmit.isNotPaid.reason.toString());
          data.append("nextCollectionDate", itemToSubmit.isNotPaid.nextCollectionDate);
          data.append("information", itemToSubmit.isNotPaid.information);
          data.append("isCanceled", "1");
        }
      }else{
        data.append("reason", itemToSubmit.isNotPaid.reason.toString());
        data.append("nextCollectionDate", itemToSubmit.isNotPaid.nextCollectionDate);
        data.append("isCanceled", "2");
      }

      const headers = new Headers();
      headers.append('jwttoken', jwttoken);

      let config = {
        method: 'POST',
        body: data,
        headers: headers,
        credentials: 'same-origin'
      }

      return API.submitPaymentTask(config)
      .then((response) => {
        if (response.status == 200) {
          response.json().then((responseJson) => {
            if (responseJson.result == "Y") {
              itemToSubmit.taskMedia.map((data, key) => {
                let last = false;
                if(key == itemToSubmit.taskMedia.length - 1){
                  last = true;
                }
                dispatch(uploadImage(jwttoken, tokenId, responseJson.refNo, data.imageType, data.imgBase64, last));
              })
            } else {
              Alert.alert(responseJson.code, responseJson.message)
            }
          })
        } else if (response.status == 401) {
          response.json().then((responseJson) => {
            Alert.alert("Error 401", responseJson.message);
            AsyncStorage.removeItem("storagePin");
            Actions.login();
          })
        } else {
          response.json().then((responseJson) => {
            Alert.alert("Error Net", responseJson.message);
            AsyncStorage.removeItem("storagePin");
            Actions.login()
          })
        }
      })
      .catch((error) => {
        console.log('Error fetch ', error);
        throw error;
      });
    })
  };
}

function tick(){
  return (dispatch, getState) => {
    const { globalReducer } = getState();
    let progressBar = {
      isShow: true,
      progress: globalReducer.progressBar.progress + 1
    }
    dispatch(setProgressBar(progressBar));
  }
}

export function submitTaskDetail(itemToSubmit, jwttoken, tokenId, dataUser, isVisited){
  return (dispatch, getState) => {
    const { currentPosition } = getState().globalReducer;
    let progressBar = {
      isShow: true,
      progress: 0
    };
    dispatch(setProgressBar(progressBar))
    dispatch(setMessageLoad("Uploading Task"));
    // this.interval = setInterval(() => dispatch(tick()), 1000);
    const deviceRefNo = dispatch(getRefNumber(dataUser.userName));

    const lat = currentPosition.lat;
    const lng = currentPosition.lng;
    
    const data = new FormData();
    data.append("tokenId", tokenId);
    data.append("lat", lat.toString());
    data.append("lng", lng.toString());
    data.append("deviceRefNo", deviceRefNo);
    data.append("distributionId", itemToSubmit.distributionId.toString());
    data.append("invoiceNo", itemToSubmit.invoiceNumber);
    data.append("onlineFlag", "1");
    data.append("trxDateMobile", itemToSubmit.trxDateMobile);
    data.append("activityId", itemToSubmit.activityId.toString());
    data.append("information", itemToSubmit.information);
    
    if(isVisited){
      data.append("namaPenerima", itemToSubmit.namaPenerima);
      data.append("reasonVisit", itemToSubmit.reasonVisit.toString());
      data.append("reasonStatus", itemToSubmit.reasonStatus.toString());
      data.append("NoTelp", itemToSubmit.NoTelp);
      data.append("isCanceled", "1");
    }else{
      data.append("reason", itemToSubmit.isNotPaid.reason);
      data.append("nextCollectionDate", itemToSubmit.isNotPaid.nextCollectionDate);
      data.append("isCanceled", "2");
    }

    const headers = new Headers();
    headers.append('jwttoken', jwttoken);

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }

    return API.submitDetailTask(config)
    .then((response) => {
      progressBar = {
        isShow: false,
        progress: 1
      }
      dispatch(setProgressBar(progressBar));
      // clearInterval(this.interval);
      if (response.status == 200) {
        response.json().then((responseJson) => {
          if (responseJson.result == "Y") {
            itemToSubmit.taskMedia.map((data, key) => {
              let last = false;
              if(key == itemToSubmit.taskMedia.length - 1){
                last = true;
              }
              dispatch(uploadImage(jwttoken, tokenId, responseJson.refNo, data.imageType, data.imgBase64, last));
            })
            // dispatch(uploadImage(jwttoken, tokenId, responseJson.refNo, itemToSubmit.taskMedia[0].imageType, itemToSubmit.taskMedia[0].imgBase64, true));
          } else {
            Alert.alert(responseJson.code, responseJson.message)
          }
        })
      } else if (response.status == 401) {
        response.json().then((responseJson) => {
          Alert.alert("Error 401", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login();
        })
      } else {
        response.json().then((responseJson) => {
          Alert.alert("Error Net", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login()
        })
      }
    })
    .catch((error) => {
      console.log('Error fetch ', error);
      throw error;
    });
  }
}

export function uploadImage(jwttoken, tokenId, refNo, imageType, imgUri, last) {
// export function uploadImage(jwttoken, tokenId, refNo, taskMedia, last){
  return dispatch => {
    dispatch(setMessageLoad("Uploading media"));
    let progressBar = {
      isShow: true,
      progress: 0
    };
    dispatch(setProgressBar(progressBar))

    const data = new FormData();
    data.append('tokenId', tokenId);
    data.append('refNo', refNo);
    data.append('imageType', imageType);
    data.append('contents', imgUri);

    const headers = new Headers();
    headers.append('jwttoken', jwttoken);

    console.log("data ", data);
    console.log("headers ", headers);

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }
    // this.interval = setInterval(() => dispatch(tick()), 1000);

    return API.uploadImage(config)
    .then((response) => {
      console.log(response)
      if (response.status == 200) {
        response.json().then((responseJson) => {
          if (responseJson.result == "Y") {
            if(last){
              Actions.home();
              dispatch(setMessageLoad(null));
              dispatch(setTaskMedia([]));
              progressBar = {
                isShow: false,
                progress: 1
              }
            }else{
              progressBar = {
                isShow: true,
                progress: 1
              }  
            }
            // clearInterval(this.interval);
          } else {
            Alert.alert(responseJson.code, responseJson.message)
          }
        })
      } else if (response.status == 401) {
        response.json().then((responseJson) => {
          Alert.alert("Error 401", responseJson.message);
          AsyncStorage.removeItem("storagePin");
          Actions.login();
        })
      } else {
        response.json().then((responseJson) => {
          Alert.alert("Error Net", responseJson.message);
          Actions.home();
        })
      }
    })
    .catch((error) => {
      console.log('Error fetch ', error);
      throw error;
    });
  }
}