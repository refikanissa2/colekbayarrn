import {
  SET_DEVICE_INFO, SET_PHONE_NO
} from '../actions/loginAction'

const initialState = {
  deviceInfo: {
    IMEI: null,
    IMSI: null,
    deviceId: null
  },
  phoneNo: null
}

export function loginReducer(state = initialState, action) {
  switch (action.type) {
    case SET_DEVICE_INFO:
      return {
        ...state,
        deviceInfo: action.deviceInfo
      }
    break;
    case SET_PHONE_NO:
      return {
        ...state,
        phoneNo: action.phoneNo
      }
    break;
    default:
      return state;
  }
}
