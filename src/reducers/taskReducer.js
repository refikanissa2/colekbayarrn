import {
  SET_TRX_LIST, SET_REFRESH_LIST, SET_ALL_REASON, SET_PAYMENT_TYPE,
  SET_SURVEY_LIST, SET_BANK_LIST, SET_IMAGE_TYPE, SET_LIST_PREDEFINE,
  SET_TASK_MEDIA, SET_MESSAGE_LOAD, SET_SETTLE_LIST
} from '../actions/taskAction'

const initialState = {
  isRefreshing: false,
  messageLoad: null,
  taskMedia: [],
  dataSettle: []
}

export function taskReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TRX_LIST:
      return {
        ...state,
        trxList: action.trxList
      }
    break;
    case SET_SETTLE_LIST:
      return {
        ...state,
        dataSettle: action.dataSettle
      }
    break;
    case SET_REFRESH_LIST:
      return {
        ...state,
        isRefreshing: action.isRefreshing
      }
    break;
    case SET_ALL_REASON:
      return {
        ...state,
        allReason: action.allReason
      }
    break;
    case SET_PAYMENT_TYPE:
      return {
        ...state,
        paymentType: action.paymentType
      }
    break;
    case SET_SURVEY_LIST:
      return {
        ...state,
        surveyList: action.surveyList
      }
    break;
    case SET_BANK_LIST:
      return {
        ...state,
        bankList: action.bankList
      }
    break;
    case SET_IMAGE_TYPE:
      return {
        ...state,
        imageType: action.imageType
      }
    break;
    case SET_LIST_PREDEFINE:
      return {
        ...state,
        listPredefine: action.listPredefine
      }
    break;
    case SET_TASK_MEDIA:
      return {
        ...state,
        taskMedia: action.taskMedia
      }
    break;
    case SET_MESSAGE_LOAD:
      return {
        ...state,
        messageLoad: action.message
      }
    break;
    default:
      return state;
  }
}
