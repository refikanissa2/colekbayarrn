import { combineReducers } from 'redux'
import { ActionConst } from 'react-native-router-flux'

import { loginReducer } from './loginReducer'
import { globalReducer } from './globalReducer'
import { taskReducer } from './taskReducer'

const sceneReducer = (state = {}, {type, scene}) => {
    switch(type){
        case ActionConst.FOCUS:
            return { ...state, scene };
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    sceneReducer,
    globalReducer,
    loginReducer,
    taskReducer
});

export default rootReducer;