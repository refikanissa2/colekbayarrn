import {
  SET_TOKEN_ID, SET_LOADING, SET_DATA_USER, SET_PROGRESS_BAR, SET_CURRENT_POSITION
} from '../actions/globalAction'

const initialState = {
  isLoading: false,
  progressBar: {
    isShow: false,
    progress: 0
  },
  currentPosition: {
    lat: null,
    lng: null
  }
}

export function globalReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TOKEN_ID:
      return {
        ...state,
        tokenId: action.tokenId
      };
    break;
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      }
    break;
    case SET_PROGRESS_BAR:
      return {
        ...state,
        progressBar: action.progressBar
      }
    break;
    case SET_CURRENT_POSITION:
      return {
        ...state,
        currentPosition: action.position
      }
    break;
    case SET_DATA_USER:
      return {
        ...state,
        dataUser: action.dataUser
      }
    break;
    default:
        return state;
  }
}
