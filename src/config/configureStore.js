import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger'
import rootReducer from '../reducers/rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

// const middlewares = [];

// if (process.env.NODE_ENV === `development`) {
//   const { logger } = require(`redux-logger`);
//   middlewares.push(ReduxThunk);
//   middlewares.push(logger);
// }else{
//   middlewares.push(ReduxThunk)
// }

export default function configureStore(preloadedState){
  const middlewares = [];
  if (process.env.NODE_ENV === `development`) {
    const { logger } = require(`redux-logger`);
    middlewares.push(ReduxThunk);
    middlewares.push(logger);
  }else{
    middlewares.push(ReduxThunk);
  }
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const storeEnhancers = [middlewareEnhancer];

  // const composedEnhancer = compose(...storeEnhancers);
  const composedEnhancer = composeWithDevTools(...storeEnhancers);

  const store = createStore(rootReducer, preloadedState, composedEnhancer);

  if(process.env.NODE_ENV !== "production") {
    if(module.hot) {
        module.hot.accept("../reducers/rootReducer", () =>{
            const newRootReducer = require("../reducers/rootReducer").default;
            store.replaceReducer(newRootReducer)
        });
    }
  }

  return store;
}

// export const store = createStore(appReducer, {}, applyMiddleware(...middlewares));