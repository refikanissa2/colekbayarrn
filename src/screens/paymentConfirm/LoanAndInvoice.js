/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import {
  List, ListItem,
  Button
} from 'native-base'
import { FormattedCurrency } from 'react-native-globalize'
import { Colors } from '../../../assets/styles/Colors'

export default class LoanAndInvoice extends Component {
  render() {
    return (
      <View>
        <ListItem>
          <View style={{flexDirection: 'column'}}>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Outlet Name</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.outletName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Customer Name</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.customerName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Distribution ID</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.distributionId}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Invoice Number</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.invoiceNumber}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Invoice Amount</Text>
              <FormattedCurrency
                value={this.props.itemToSubmit.invoiceAmount}
                currency="IDR"
                style={[styles.text, styles.textValue]}
                maximumFractionDigits={2}
              />
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Information</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.information}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Visit Status</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.visitStatusName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Meet</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.meetName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Intention To Pay</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.intentionToPayName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Debtor</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.debtorName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Debtor Cooperative</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.debtorCoopName}</Text>
            </View>
          </View>
        </ListItem>
        {this.props.itemToSubmit.paymentType.map((data, key) => {
          if(data.p == "CSH"){
            return(
              <ListItem key={key}>
                <View style={{flexDirection: 'column'}}>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>CASH</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField]}>Amount</Text>
                    <FormattedCurrency
                      value={data.a}
                      currency="IDR"
                      style={[styles.text, styles.textValue]}
                      maximumFractionDigits={2}
                    />
                  </View>
                </View>
              </ListItem>
            )
          }else if(data.p == "TRF"){
            return(
              <ListItem key={key}>
                <View style={{flexDirection: 'column'}}>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>TRANSFER</Text>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: '75%', flexDirection: 'column'}}>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Amount</Text>
                        <FormattedCurrency
                          value={data.a}
                          currency="IDR"
                          style={[styles.text, styles.textValue]}
                          maximumFractionDigits={2}
                        />
                      </View>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Bank</Text>
                        <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.bankTRFName}</Text>
                      </View>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Account Owner</Text>
                        <Text style={[styles.text, styles.textValue]}>{data.r}</Text>
                      </View>
                    </View>
                    <View style={{width: '25%', alignItems: 'flex-end', justifyContent: 'flex-start', flexDirection: 'column'}}>
                      <Image source={{uri: this.props.itemToSubmit.taskMedia.filter(x => x.imageType == 'IMG1').map(x => x.imgUri)[0]}} style={{width: 60, height: 80}} resizeMode="stretch"/>
                    </View>
                  </View>
                </View>
              </ListItem>
            )
          }else if(data.p == "CHQ"){
            return(
              <ListItem key={key}>
                <View style={{flexDirection: 'column'}}>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>CHEQUE</Text>
                  </View>
                  <View style={styles.row}>
                    <View  style={{width: '75%', flexDirection: 'column'}}>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Amount</Text>
                        <FormattedCurrency
                          value={data.a}
                          currency="IDR"
                          style={[styles.text, styles.textValue]}
                          maximumFractionDigits={2}
                        />
                      </View>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Bank</Text>
                        <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.bankCHQName}</Text>
                      </View>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Date</Text>
                        <Text style={[styles.text, styles.textValue]}>{data.d}</Text>
                      </View>
                      <View style={styles.row}>
                        <Text style={[styles.text, styles.textField]}>Cheque Number</Text>
                        <Text style={[styles.text, styles.textValue]}>{data.r}</Text>
                      </View>
                    </View>
                    <View style={{width: '25%', alignItems: 'flex-end', justifyContent: 'flex-start', flexDirection: 'column'}}>
                      <Image source={{uri: this.props.itemToSubmit.taskMedia.filter(x => x.imageType == 'IMG2').map(x => x.imgUri)[0]}} style={{width: 60, height: 80}} resizeMode="stretch"/>
                    </View>
                  </View>
                </View>
              </ListItem>
            )
          }else if(data.p == "RTR"){
            return(
              <ListItem key={key}>
                <View style={{flexDirection: 'column'}}>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>RETUR</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField]}>Amount</Text>
                    <FormattedCurrency
                      value={data.a}
                      currency="IDR"
                      style={[styles.text, styles.textValue]}
                      maximumFractionDigits={2}
                    />
                  </View>
                  <View style={styles.row}>
                    <Text style={[styles.text, styles.textField]}>Invoice Number</Text>
                    <Text style={[styles.text, styles.textValue]}>{data.r}</Text>
                  </View>
                </View>
              </ListItem>
            )
          }
        })}
        {this.props.itemToSubmit.isPartial.reason != "" &&
          <ListItem>
            <View style={{flexDirection: 'column'}}>
              <View style={styles.row}>
                <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>PARTIAL PAYMENT</Text>
              </View>
              <View style={styles.row}>
                <Text style={[styles.text, styles.textField]}>Partial Reason</Text>
                <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.partialPaymentName}</Text>
              </View>
              <View style={styles.row}>
                <Text style={[styles.text, styles.textField]}>Next Payment Date</Text>
                <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.isPartial.nextCollectionDate}</Text>
              </View>
            </View>
          </ListItem>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginBottom: 10
  },
  text: {
    fontSize: 14
  },
  textField: {
    width: 150
  },
  textValue: {
    width: 200
  }
});
