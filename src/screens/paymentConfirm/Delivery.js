/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import {
  List, ListItem,
  Button
} from 'native-base'
import { FormattedCurrency } from 'react-native-globalize'
import { Colors } from '../../../assets/styles/Colors'

export default class Delivery extends Component {
  render() {
    return (
      <View>
        <ListItem>
          <View style={{flexDirection: 'column'}}>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Outlet Name</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.outletName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Customer Name</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.customerName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Distribution ID</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.distributionId}</Text>
            </View>
          </View>
        </ListItem>
        <ListItem>
          <View style={{flexDirection: 'column'}}>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>DELIVERY</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Visit Status</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.visitStatusName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Meet</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.meetName}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Information</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.information}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Recipient Name</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.namaPenerima}</Text>
            </View>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField]}>Phone Number</Text>
              <Text style={[styles.text, styles.textValue]}>{this.props.itemToSubmit.NoTelp}</Text>
            </View>
          </View>
        </ListItem>
        <ListItem>
          <View style={{flexDirection: 'column'}}>
            <View style={styles.row}>
              <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>MEDIA</Text>
            </View>
            <View style={styles.row}>
              <Image source={{uri: this.props.itemToSubmit.taskMedia.filter(x => x.imageType == 'DETS').map(x => x.imgUri)[0]}} style={{height: 100, width: 100, alignSelf: 'flex-start'}}/>
            </View>
          </View>
        </ListItem>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginBottom: 10
  },
  text: {
    fontSize: 14
  },
  textField: {
    width: 150
  },
  textValue: {
    width: 200
  }
});
