import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  TouchableHighlight
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title
} from "native-base";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { FormattedCurrency, FormattedWrapper } from "react-native-globalize";
import CheckBox from "react-native-check-box";

import { Colors } from "../../assets/styles/Colors";
import {
  submitSettle,
  getSettleList,
  setSettleList
} from "../actions/taskAction";

mapStateToProps = state => ({
  dataSettle: state.taskReducer.dataSettle
});

mapDispatchToProps = dispatch => ({
  submitSettle: (jwttoken, tokenId, refNo, last) => {
    dispatch(submitSettle(jwttoken, tokenId, refNo, last));
  },
  getSettleList: (jwttoken, tokenId) => {
    dispatch(getSettleList(jwttoken, tokenId));
  },
  setSettleList: dataSettle => {
    dispatch(setSettleList(dataSettle));
  }
});

class SettleScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadSubmit: false
    };
  }
  componentWillMount() {
    this.props.getSettleList(this.props.jwttoken, this.props.tokenId);
  }
  render() {
    return (
      <FormattedWrapper
        locale="id"
        cldr={[require("../components/formatCurrency/cldr.json")]}
      >
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={{ width: "10%", alignItems: "center" }}>
              <TouchableHighlight activeOpacity={0.9} onPress={() => Actions.pop()}>
                <View>
                  <Icon name="arrow-back" style={{ color: "white" }} />
                </View>
              </TouchableHighlight>
            </View>
            <View
              style={{ width: "80%", alignItems: "center", marginLeft: -15 }}
            >
              <Text
                style={{ color: "white", fontSize: 16, fontWeight: "bold" }}
              >
                Settle Transaction
              </Text>
            </View>
            <View style={{ width: "10%", alignItems: "center" }}>
              <TouchableHighlight activeOpacity={0.9} onPress={() => this.clickSubmitSettle()}>
                <View>
                  {this.state.isLoadSubmit ? (
                    <ActivityIndicator color={"white"} size={25} />
                  ) : (
                    <Icon name="md-checkmark" style={{ color: "white" }} />
                  )}
                </View>
              </TouchableHighlight>
            </View>
          </View>
          {this.props.dataSettle.length == 0 ? (
            <ActivityIndicator color={Colors.COLEKBAYAR} size={30} />
          ) : (
            <View style={{ flex: 1, padding: 10 }}>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.text}>Date :</Text>
                </View>
                <View style={{ flex: 1, alignItems: "flex-end" }}>
                  <Text style={styles.text}>{this.props.dataSettle.date}</Text>
                </View>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.text}>Total :</Text>
                </View>
                <View style={{ flex: 1, alignItems: "flex-end" }}>
                  <FormattedCurrency
                    value={this.props.dataSettle.totalSettlement}
                    currency="IDR"
                    maximumFractionDigits={2}
                    style={styles.text}
                  />
                </View>
              </View>
              <View
                style={{
                  height: 1,
                  width: "95%",
                  backgroundColor: Colors.GREY,
                  margin: 10
                }}
              />
              <FlatList
                data={this.props.dataSettle.trxList}
                renderItem={({ item, index }) => (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row"
                    }}
                  >
                    <View style={{ width: 25, marginRight: 10, marginTop: 5 }}>
                      <CheckBox
                        isChecked={this.props.dataSettle.settleChecked[index]}
                        onClick={() =>
                          this.checkSettleList(
                            this.props.dataSettle.settleChecked[index],
                            index
                          )
                        }
                        checkBoxColor={Colors.COLEKBAYAR}
                      />
                    </View>
                    <View style={{ flexDirection: "column" }}>
                      <View>
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                          {item.customerName}
                        </Text>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: 14, marginRight: 15 }}>
                          {item.invoiceNo}
                        </Text>
                        <FormattedCurrency
                          value={item.trxAmount}
                          currency="IDR"
                          maximumFractionDigits={2}
                          style={styles.text}
                        />
                      </View>
                    </View>
                  </View>
                )}
                keyExtractor={(item, index) => index}
                ItemSeparatorComponent={this._renderSeparator()}
              />
            </View>
          )}
        </View>
      </FormattedWrapper>
    );
  }

  checkSettleList(checked, index) {
    let tmpDataSettle = this.props.dataSettle;
    tmpDataSettle.settleChecked[index] = !checked;
    this.props.setSettleList(tmpDataSettle);
  }

  clickSubmitSettle() {
    let dataSettle = this.props.dataSettle.settleChecked;
    dataSettle.map((data, index) => {
      this.setState({ isLoadSubmit: true });
      if (data) {
        let trxList = this.props.dataSettle.trxList[index];
        let refNo = trxList.refNo;
        if (index == dataSettle.length - 1) {
          this.props.submitSettle(
            this.props.jwttoken,
            this.props.tokenId,
            refNo,
            true
          );
        } else {
          this.props.submitSettle(
            this.props.jwttoken,
            this.props.tokenId,
            refNo,
            false
          );
        }
      } else {
        this.setState({ isLoadSubmit: false });
      }
    });
  }

  _renderSeparator() {
    return (
      <View
        style={{
          height: 1,
          width: "80%",
          backgroundColor: Colors.GREY,
          marginLeft: "14%"
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    backgroundColor: Colors.COLEKBAYAR,
    padding: 10,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    flexDirection: "row"
  },
  text: {
    fontSize: 15
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SettleScreen);
