/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux'
import { getActivityList } from '../actions/activityAction'

mapStateToProps = (state) => ({

})

mapDispatchToProps = (dispatch) => ({
  getActivityList: (jwttoken) => {
    dispatch(getActivityList(jwttoken));
  }
})

class ActivityScreen extends Component {
  componentWillMount(){
    AsyncStorage.getItem("jwttoken", (err, result) => {
      this.props.getActivityList(result);
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>I'm the ActivityScreen component</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityScreen);