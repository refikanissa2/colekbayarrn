/* @flow */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  ImageBackground,
  Dimensions,
  View,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import {
  Container,
  Tabs, Tab, TabHeading,
  StyleProvider,
} from 'native-base'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Marker from 'react-native-image-marker'
import { FormattedWrapper } from 'react-native-globalize'
var { height, width } = Dimensions.get('window')

import { Colors } from '../../assets/styles/Colors'
import Delivery from './submitTask/Delivery'

import { setTaskMedia } from '../actions/taskAction'

mapStateToProps = (state) => ({
})

mapDispatchToProps = (dispatch) => ({
  setTaskMedia: (taskMedia) => {
    dispatch(setTaskMedia(taskMedia));
  }
})

class SubmitDetailTask extends Component {
  componentWillMount(){
    let taskMedia = [];
    this.props.setTaskMedia(taskMedia)
    Actions.refresh({title: this.props.outletName});
  }

  render() {
    return (
      <FormattedWrapper locale="id" cldr={[require('../components/formatCurrency/cldr.json')]}>
      <StyleProvider style={getTheme(material)}>
        <View style={{flex:1}}>
          <Tabs locked={true} tabBarUnderlineStyle={{backgroundColor: 'rgba(0,0,0,0)'}}>
            <Tab heading={
                <TabHeading tabStyle={{backgroundColor: Colors.COLEKBAYAR}}><Icon name="attach-money" size={22} color='#fff'/><Text style={styles.textTabMenu}>Delivery</Text></TabHeading>
              }
            >
              <Delivery detailItem={this.props}/>
            </Tab>
          </Tabs>
        </View>
      </StyleProvider>
      </FormattedWrapper>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitDetailTask);

const styles = StyleSheet.create({
  textTabMenu: {
    color: '#fff',
    fontSize: 16
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 1,
    height: height,
    width: width,
    paddingBottom: 80
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
    margin: 40
  }
});
