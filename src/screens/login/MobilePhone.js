/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import { Colors } from '../../../assets/styles/Colors'
import { submitLogin } from '../../actions/loginAction'

mapStateToProps = (state) => ({
  phoneNo: state.loginReducer.phoneNo,
  deviceInfo: state.loginReducer.deviceInfo,
  isLoading: state.globalReducer.isLoading
})

mapDispatchToProps = (dispatch) => ({
  submitLogin: (imsi, imei, deviceId, phoneNo) => {
    dispatch(submitLogin(imsi, imei, deviceId, phoneNo));
  }
})

class MobilePhone extends Component {
  constructor(props){
    super(props)
    this.state={
      phoneNo: this.props.phoneNo
    }
  }

  render() {
    const { deviceInfo, isLoading } = this.props;
    return (
      <View style={{margin: 25, flex: 1, flexDirection: 'column'}}>
        <KeyboardAvoidingView behavior="padding">
          <View>
            <TextInput onChangeText={(text) => this.setState({phoneNo: text})}
              returnKeyType={"done"}
              onSubmitEditing={() => this.props.submitLogin(deviceInfo.IMSI, deviceInfo.IMEI, deviceInfo.deviceId, this.state.phoneNo)}
              value={this.state.phoneNo}
              keyboardType={"phone-pad"}
              placeholder="Masukkan No Telp"
              style={{textAlign: 'center'}}
            />
          </View>
          <View style={{marginTop: 10}}>
            <TouchableOpacity onPress={() => this.props.submitLogin(deviceInfo.IMSI, deviceInfo.IMEI, deviceInfo.deviceId, this.state.phoneNo)}
                style={{backgroundColor: Colors.COLEKBAYAR, padding: 10, alignItems: 'center'}}
            >
              <View>
                {isLoading ? (
                  <ActivityIndicator color="white" size={30}/>
                ):(
                  <Text style={{color: "white", fontWeight: 'bold'}}>SUBMIT</Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MobilePhone)
