/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
  Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux'

import { Colors } from '../../../assets/styles/Colors'

export default class CreatePin extends Component {
  constructor(props){
    super(props)
    this.state={
      pin1: null,
      pin2: null
    }
  }
  render() {
    return (
      <View style={{margin: 25, flex: 1, flexDirection: 'column'}}>
        <KeyboardAvoidingView behavior="padding">
          <View>
            <TextInput ref={(ref) => { this._pin = ref; }}
              returnKeyType={"next"}
              onSubmitEditing={(event) => {this.focusConfirmInput()}}
              onChangeText={(text) => this.setState({pin1: text})}
              keyboardType={"numeric"}
              placeholder="Create 6 digit PIN"
              secureTextEntry={true}
              maxLength={6}
              style={{textAlign: 'center'}}
            />
          </View>
          <View>
            <TextInput ref={(ref) => { this._pinConfirm = ref; }}
              returnKeyType={"done"}
              onSubmitEditing={this.submitButtonClick.bind(this)}
              onChangeText={(text) => this.setState({pin2: text})}
              keyboardType={"numeric"}
              placeholder="Create 6 digit PIN"
              secureTextEntry={true}
              maxLength={6}
              style={{textAlign: 'center'}}
            />
          </View>
          <View style={{marginTop: 10}}>
            <TouchableOpacity onPress={this.submitButtonClick.bind(this)}
                style={{backgroundColor: Colors.COLEKBAYAR, padding: 10, alignItems: 'center'}}
            >
              <View>
                  <Text style={{color: "white", fontWeight: 'bold'}}>SUBMIT</Text>
              </View>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }

  focusConfirmInput(){
    this._pinConfirm.focus()
  }

  submitButtonClick(){
    var pin2 = this.state.pin2
    var pin1 = this.state.pin1
    if(pin2 === pin1){
      AsyncStorage.setItem("storagePin", pin1);
      Actions.refresh({statusLogin: 'loginPin'});
    }else{
      Alert.alert("Failed", "Pin doesn't match");
    }
  }
}
