/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  NativeModules,
  AsyncStorage,
  Alert
} from 'react-native';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import { Colors } from '../../../assets/styles/Colors'
import { setTokenId } from '../../actions/globalAction'
import { getDeviceInfo } from '../../actions/loginAction'

mapStateToProps = (state) => ({
  deviceInfo: state.loginReducer.deviceInfo,
  isLoading: state.globalReducer.isLoading
})

mapDispatchToProps = (dispatch) => ({
  setTokenId: (tokenId) => {
    dispatch(setTokenId(tokenId));
  },
  getDeviceInfo: () => {
    dispatch(getDeviceInfo());
  }
})

class LoginPin extends Component {
  constructor(props){
    super(props)
    this.state={
      pinLogin: null
    }
  }

  componentWillMount(){
    AsyncStorage.getItem("storagePin", (error, result) => {
      this.setState({
        storagePin: result
      })
    })
    this.props.getDeviceInfo()
  }

  render() {
    return (
      <View style={{margin: 25, flex: 1, flexDirection: 'column'}}>
        <KeyboardAvoidingView behavior="padding">
          <View>
            <TextInput onChangeText={(text) => this.setState({pinLogin: text})}
              returnKeyType={"done"}
              onSubmitEditing={this.loginPin.bind(this)}
              value={this.state.pinLogin}
              keyboardType={"numeric"}
              placeholder="Masukkan pin"
              style={{textAlign: 'center'}}
              secureTextEntry={true}
              maxLength={6}
            />
          </View>
          <View style={{marginTop: 10}}>
            <TouchableOpacity onPress={this.loginPin.bind(this)}
                style={{backgroundColor: Colors.COLEKBAYAR, padding: 10, alignItems: 'center'}}
            >
              <View>
                {this.props.isLoading ? (
                  <ActivityIndicator color="white" size={30}/>
                ):(
                  <Text style={{color: "white", fontWeight: 'bold'}}>LOGIN</Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }

  loginPin(){
    if(this.state.pinLogin === this.state.storagePin){
      const imei = this.props.deviceInfo.IMEI;
      const imsi = this.props.deviceInfo.IMSI;
      const deviceId = this.props.deviceInfo.deviceId;
      let combinedDeviceInfo = imei+deviceId+imsi;
      NativeModules.IMEI.md5(combinedDeviceInfo).then(result => {
        this.props.setTokenId(result);
      })
      Alert.alert("Success", "Login Success !");
      Actions.home();
    }else{
      Alert.alert("Failed", "Login Failed !")
      this.setState({pinLogin: null})
      Actions.refresh({statusLogin: "loginPin"})
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPin);
