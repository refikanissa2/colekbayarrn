/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';
import {connect} from 'react-redux'

import { Colors } from '../../../assets/styles/Colors'
import { activate } from '../../actions/loginAction'

mapStateToProps = (state) => ({
  phoneNo: state.loginReducer.phoneNo,
  isLoading: state.globalReducer.isLoading
})

mapDispatchToProps = (dispatch) => ({
  activate: (phoneNo, tokenotp, jwttoken) => {
    dispatch(activate(phoneNo, tokenotp, jwttoken));
  }
})

class OTP extends Component {
  constructor(props){
    super(props)
    this.state={
      tokenotp: null
    }
  }

  componentWillMount(){
    AsyncStorage.getItem("jwttoken", (err, result) => {
      this.setState({jwttoken: result})
    })
  }

  render() {
    return (
      <View style={{margin: 25, flex: 1, flexDirection: 'column'}}>
        <KeyboardAvoidingView behavior="padding">
          <View>
            <TextInput onChangeText={(text) => this.setState({tokenotp: text})}
              returnKeyType={"done"}
              onSubmitEditing={() => this.props.activate(this.props.phoneNo, this.state.tokenotp, this.state.jwttoken)}
              value={this.state.tokenotp}
              keyboardType={"numeric"}
              placeholder="Enter OTP Token"
              style={{textAlign: 'center'}}
            />
          </View>
          <View style={{marginTop: 10}}>
            <TouchableOpacity onPress={() => this.props.activate(this.props.phoneNo, this.state.tokenotp, this.state.jwttoken)}
                style={{backgroundColor: Colors.COLEKBAYAR, padding: 10, alignItems: 'center'}}
            >
              <View>
                {this.props.isLoading ? (
                  <ActivityIndicator color="white" size={30}/>
                ):(
                  <Text style={{color: "white", fontWeight: 'bold'}}>SUBMIT</Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OTP);
