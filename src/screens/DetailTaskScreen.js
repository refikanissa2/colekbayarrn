/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  BackHandler,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import {
  List, ListItem, Separator,
  Body, Right,
} from 'native-base'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { FormattedCurrency, FormattedWrapper } from 'react-native-globalize'

import { Colors } from '../../assets/styles/Colors'

export default class DetailTaskScreen extends Component {
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this._backToMenu);
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this._backToMenu);
  }

  _backToMenu(){
    Actions.pop();
    return true;
  }

  render() {
    if(this.props.item.invoiceType == "detail"){
      let indexDetailBarang = this.props.item.aktivitas.map(x => x.aktivitasName).indexOf("DETS");
      let detailBarang = this.props.item.aktivitas[indexDetailBarang].detailBarang;
    }
    return (
      <FormattedWrapper locale="id" cldr={[require('../components/formatCurrency/cldr.json')]}>
        <View style={styles.container}>
          <ScrollView>
            <List>
              <ListItem>
                <Body>
                  <Text>{this.props.item.outletName}</Text>
                </Body>
                <Right>
                  <View style={{flexDirection: 'row'}}>
                    <Icon name="call" size={25} style={{padding: 5}} color='#2C77BC'/>
                    <Icon name="location-on" size={25} style={{padding: 5}} color='#2C77BC'/>
                  </View>
                </Right>
              </ListItem>
              <Separator bordered>
                <Text style={styles.textSeparator}>CUSTOMER DETAIL</Text>
              </Separator>
              <ListItem>
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Customer Name</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.customerName}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Person in Charge</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.pic}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Full Address</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.address1} RT {this.props.item.rt} RW {this.props.item.rw}, {this.props.item.city}, {this.props.item.province}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Address 1</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.address1}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Address 2</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.address2}</Text>
                  </View>
                </View>
              </ListItem>
              <Separator bordered>
                <Text style={styles.textSeparator}>TASK DETAIL</Text>
              </Separator>
              <ListItem>
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Invoice Number</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.invoiceNo}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Invoice Date</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.invoiceDate}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Type</Text>
                    <Text style={[styles.text, styles.textValue]}>{this.props.item.invoiceType.toUpperCase()}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Total Amount</Text>
                    <FormattedCurrency
                      value={this.props.item.totalAmount}
                      currency="IDR"
                      style={[styles.text, styles.textValue]}
                      maximumFractionDigits={2}
                    />
                  </View>
                </View>
              </ListItem>
              {this.props.item.invoiceType == "detail" &&
              <View>
                <Separator bordered>
                  <Text style={styles.textSeparator}>PRODUCTS TO DELIVER</Text>
                </Separator>
                <ListItem>
                  <View style={{flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={[styles.text, styles.textQty]}>Quantity</Text>
                      <Text style={[styles.text, styles.textDesc]}>Description</Text>
                      <Text style={[styles.text, styles.textPrice]}>Price</Text>
                    </View>
                    {detailBarang.map((data, index) => {
                      return(
                        <View style={{flexDirection: 'row'}} key={index}>
                          <Text style={[styles.text, styles.textQty]}>{data.jumlah}</Text>
                          <Text style={[styles.text, styles.textDesc]}>{data.deskripsi}</Text>
                          <FormattedCurrency
                            value={data.harga}
                            currency="IDR"
                            style={[styles.text, styles.textPrice]}
                            maximumFractionDigits={2}
                          />
                        </View>
                      )
                    })}
                  </View>
                </ListItem>
              </View>
              }
              <ListItem style={{alignItems: 'center', justifyContent:'center'}}>
                  <TouchableOpacity style={{width: 200, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.COLEKBAYAR, padding: 10}}
                    onPress={() => Actions.submitDetail(this.props.item)}
                  >
                    <View>
                      <Text style={{color: 'white', fontWeight: 'bold'}}>START</Text>
                    </View>
                  </TouchableOpacity>
              </ListItem>
            </List>
          </ScrollView>
        </View>
      </FormattedWrapper>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  textField: {
    width: 150
  },
  textValue: {
    width: 200
  },
  textSeparator: {
    fontSize: 15,
    color: '#2C77BC',
    fontWeight: 'bold'
  },
  text: {
    fontSize: 13
  },
  textQty: {
    width: 75
  },
  textDesc: {
    width: 125
  },
  textPrice: {
    width: 100
  }
});
