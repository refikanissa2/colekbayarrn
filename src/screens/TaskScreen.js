/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  CheckBox,
  RefreshControl,
  AsyncStorage,
  ToastAndroid
} from 'react-native';
import {
  Picker, Item,
  Content,
  Radio,
} from 'native-base';
var PickerItem = Picker.Item;
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'

import Icon from 'react-native-vector-icons/MaterialIcons'

import { Colors } from '../../assets/styles/Colors'
import { getTaskList } from '../actions/taskAction'
import { getCurrentPosition } from '../actions/globalAction'

mapStateToProps = (state) => ({
  isLoading: state.globalReducer.isLoading,
  tokenId: state.globalReducer.tokenId,
  isRefreshing: state.taskReducer.isRefreshing,
  trxList: state.taskReducer.trxList,
  allReason: state.taskReducer.allReason
})

mapDispatchToProps = (dispatch) => ({
  getTaskList: (jwttoken, tokenId, isRefreshControl) => {
    dispatch(getTaskList(jwttoken, tokenId, isRefreshControl));
  },
  getCurrentPosition: () => {
    dispatch(getCurrentPosition());
  }
})

class TaskScreen extends Component {
  // constructor(props){
  //   super(props);
  //   this.state={
  //     submitModal: false,
  //     styleModal: {},
  //     listResult: [
  //       {
  //         name: "Select Result",
  //         value: ""
  //       }, {
  //         name: "Pay",
  //         value: "PAY"
  //       }, {
  //         name: "Not Pay",
  //         value:"NOT_PAY"
  //       }, {
  //         name: "Not Found",
  //         value: "NOT_FOUND"
  //       }, {
  //         name: "Not Visited",
  //         value: "NOT_VISITED"
  //       }
  //     ],
  //     selectedResult: "", selectedReason: "",
  //     isSubmit: false,
  //     isReason: false,
  //     isPromiseToPay: false,
  //     listReason: [],
  //     dateToPay: new Date(),
  //     listChecked: []
  //   }
  // }
  componentWillMount(){
    AsyncStorage.getItem("jwttoken", (err, result) => {
      this.props.getTaskList(result, this.props.tokenId, 0);
      this.setState({jwttoken: result})
    })
  }

  // componentWillUpdate(nextProps, nextState){
  //   if(this.state.submitModal != nextState.submitModal){
  //     if(!nextState.submitModal){
  //       this.setState({
  //         listChecked: [],
  //         selectedResult: "",
  //         isSubmit: false,
  //         isReason: false,
  //         selectedReason: "",
  //         isPromiseToPay: false
  //       })
  //     }
  //   }
  // }

  renderSeparator(){
    return (
      <View
        style={{
          height: 1,
          width: "80%",
          backgroundColor: Colors.GREY,
          marginLeft: "14%"
        }}
      />
    )
  }

  // async openAndroidDatePicker() {
  //   try {
  //     const {action, year, month, day} = await DatePickerAndroid.open({
  //       date: this.state.dateToPay
  //     });
  //     if (action !== DatePickerAndroid.dismissedAction) {
  //         var date = new Date(year, month, day);
  //         this.setState({dateToPay: date});
  //     }
  //   } catch ({code, message}) {
  //     console.warn('Cannot open date picker', message);
  //   }
  // }

  render() {
    // let listResult = this.state.listResult.map((data, index) => {
    //   return <PickerItem key={index+1} value={data.value} label={data.name} />
    // })
    // let listReason = this.state.listReason.map((data, index) => {
    //   return <PickerItem key={index+1} value={data.value} label={data.name} />
    // })
    return (
      <View style={styles.container}>
        {this.props.isLoading ? (
          <ActivityIndicator color={Colors.COLEKBAYAR} size={30}/>
        ):(
          <View style={{flex: 1}}>
            <FlatList
              data={this.props.trxList}
              renderItem={({item}) =>
                <TouchableOpacity onPress={() => Actions.detail({item})}>
                  <View style={{flex: 1, flexDirection: 'row', padding: 10, margin: 10}}>
                    <View style={{width: 20, marginRight: 20}}>
                      <CheckBox/>
                    </View>
                    <View style={{marginRight: 10, flex: 1}}>
                      <View style={{flexDirection: 'column'}}>
                        <View>
                          <Text style={{fontSize: 16}}>{item.outletName}</Text>
                        </View>
                        <View>
                          <Text style={{fontSize: 12}}>{item.city}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={{width: 70, flexDirection: 'row'}}>
                      <View>
                        <Icon name="call" size={25} style={{padding: 5}}/>
                      </View>
                      <View>
                        <Icon name="location-on" size={25} style={{padding: 5}}/>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              }
              keyExtractor={item => item.id}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.isRefreshing}
                  onRefresh={() => this.props.getTaskList(this.state.jwttoken, this.props.tokenId, 1)}
                />
              }
              ItemSeparatorComponent={this.renderSeparator}
            />
          </View>
        )}
      </View>
    );
  }

  // submitButtonClick(){
  //   let itemToSubmit;

  // }

  // resizeModal(ev){
  //   this.setState({styleModal: {height: ev.nativeEvent.layout.height + 30}});
  // }

  // checkBoxClick(id, customerId){
  //   var tmp = this.state.listChecked
  //   if(tmp.length > 0){
  //     if(tmp.map(x => x.id).includes(id)){
  //       tmp.splice(tmp.map(x => x.id).indexOf(id))
  //     }else{
  //       if(tmp.map(y => y.customerId).includes(customerId)){
  //         tmp.push({id: id, customerId: customerId})
  //         // this.refs.submitModal.open()
  //         this.setState({submitModal: true})
  //       }else{
  //         Alert.alert("This task is different customer")
  //       }
  //     }
  //   }else{
  //     tmp.push({id: id, customerId: customerId})
  //     // this.refs.submitModal.open()
  //     this.setState({submitModal: true})
  //   }
  //   this.setState({
  //     listChecked: tmp
  //   })
  // }

  // selectedResultChange(data){
  //   if(data == "PAY"){
  //     this.setState({
  //       isSubmit: true,
  //       isReason: false,
  //       selectedResult: data,
  //       isPromiseToPay: false
  //     })
  //   }else{
  //     let listReason = this.props.allReason.filter(item => item.type == data);
  //     this.setState({
  //       isSubmit: false,
  //       isReason: true,
  //       selectedResult: data,
  //       listReason: listReason
  //     })
  //   }
  // }

  // selectedReasonChange(data){
  //   this.setState({
  //     selectedReason: data
  //   })
  //   if(data === 256){
  //     this.setState({
  //       isPromiseToPay: true
  //     })
  //   }else{
  //     this.setState({
  //       isPromiseToPay: false
  //     })
  //   }
  //   if(data != ""){
  //     this.setState({
  //       isSubmit: true
  //     })
  //   }else{
  //     this.setState({
  //       isSubmit: false
  //     })
  //   }
  // }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  modal: {
    height: 150,
    borderTopWidth: 2,
    borderColor: 'rgba(189, 195, 199,0.5)'
  }
});
