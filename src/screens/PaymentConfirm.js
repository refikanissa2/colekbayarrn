/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ToastAndroid,
  TouchableOpacity,
  Image,
  AsyncStorage
} from 'react-native';
import {
  List, ListItem, Separator,
  Body, Right,
} from 'native-base'
import { FormattedCurrency, FormattedWrapper } from 'react-native-globalize'
import { Colors } from '../../assets/styles/Colors'
import { connect } from 'react-redux'
import { submitTaskPay, submitTaskDetail, setTaskMedia } from '../actions/taskAction'
import { setProgressBar } from '../actions/globalAction'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/Entypo';

import Delivery from './paymentConfirm/Delivery'
import LoanAndInvoice from './paymentConfirm/LoanAndInvoice'
import SignatureView from '../components/SignatureView'
import ProgressBar from '../components/ProgressBar'

mapStateToProps = (state) => ({
  allReason: state.taskReducer.allReason,
  bankList: state.taskReducer.bankList,
  tokenId: state.globalReducer.tokenId,
  dataUser: state.globalReducer.dataUser,
  taskMedia: state.taskReducer.taskMedia,
  progressBar: state.globalReducer.progressBar,
  messageLoad: state.taskReducer.messageLoad
})

mapDispatchToProps = (dispatch) => ({
  submitTaskPay: (itemToSubmit, jwttoken, tokenId, dataUser, isVisited) => {
    dispatch(submitTaskPay(itemToSubmit, jwttoken, tokenId, dataUser, isVisited));
  },
  submitTaskDetail: (itemToSubmit, jwttoken, tokenId, dataUser, isVisited) => {
    dispatch(submitTaskDetail(itemToSubmit, jwttoken, tokenId, dataUser, isVisited));
  },
  setTaskMedia: (taskMedia) => {
    dispatch(setTaskMedia(taskMedia));
  },
  setProgressBar: (progressBar) => {
    dispatch(setProgressBar(progressBar));
  }
})

class PaymentConfirm extends Component {
  constructor(props){
    super(props)
    this.state={
      showSignature: false,
      showProgress : false
    }
  }

  componentWillMount(){
    AsyncStorage.getItem("jwttoken", (err, result) => {
      this.setState({jwttoken: result})
    })
    let visit =""
    let meet=""
    let intent=""
    let debtor=""
    let debtorCoop=""
    let bankTRF=""
    let bankCHQ = ""
    let partialPayment = ""
    if(this.props.itemToSubmit.reasonStatus != ""){
      visit = this.props.allReason[
        this.props.allReason.map((x) => x.id).indexOf(this.props.itemToSubmit.reasonStatus)
      ].name;
    }

    if(this.props.itemToSubmit.reasonVisit != ""){
      meet = this.props.allReason[
        this.props.allReason.map((x) => x.id).indexOf(this.props.itemToSubmit.reasonVisit)
      ].name;
    }

    if(this.props.itemToSubmit.invoiceType != "detail"){
      if(this.props.itemToSubmit.reasonAttitude != ""){
        intent = this.props.allReason[
          this.props.allReason.map((x) => x.id).indexOf(this.props.itemToSubmit.reasonAttitude)
        ].name;
      }
  
      if(this.props.itemToSubmit.reasonDebitur != ""){
        debtor = this.props.allReason[
          this.props.allReason.map((x) => x.id).indexOf(this.props.itemToSubmit.reasonDebitur)
        ].name;
      }
  
      if(this.props.itemToSubmit.reasonIsCooperative != ""){
        debtorCoop = this.props.allReason[
          this.props.allReason.map((x) => x.id).indexOf(this.props.itemToSubmit.reasonIsCooperative)
        ].name;
      }

      if(this.props.itemToSubmit.paymentType.length > 0){
        this.props.itemToSubmit.paymentType.map((data, key) => {
          if(data.p == "TRF"){
            if(data.b != ""){
              bankTRF = this.props.bankList[
                this.props.bankList.map(x => x.id).indexOf(data.b)
              ].name;
            }
          }else if(data.p == "CHQ"){
            if(data.b != ""){
              bankCHQ = this.props.bankList[
                this.props.bankList.map(x => x.id).indexOf(data.b)
              ].name;
            }
          }
        })
      }
  
      if(this.props.itemToSubmit.isPartial.reason != ""){
        partialPayment = this.props.allReason[
          this.props.allReason.map((x) => x.id).indexOf(this.props.itemToSubmit.isPartial.reason)
        ].name;
      }
    }

    this.setState({
      visitStatus: visit,
      meet: meet,
      intentionToPay: intent,
      debtor: debtor,
      debtorCoop: debtorCoop,
      bankTRF: bankTRF,
      bankCHQ: bankCHQ,
      partialPayment: partialPayment
    })
  }
  render() {
    let invoiceType = this.props.itemToSubmit.invoiceType;
    let itemToSubmit = this.props.itemToSubmit;
    itemToSubmit.visitStatusName = this.state.visitStatus;
    itemToSubmit.meetName = this.state.meet;
    itemToSubmit.intentionToPayName = this.state.intentionToPay;
    itemToSubmit.debtorName = this.state.debtor;
    itemToSubmit.debtorCoopName = this.state.debtorCoop;
    itemToSubmit.bankTRFName = this.state.bankTRF;
    itemToSubmit.bankCHQName = this.state.bankCHQ;
    itemToSubmit.partialPaymentName = this.state.partialPayment;
    let renderComponent;
    if(invoiceType == "detail"){
      renderComponent = <Delivery itemToSubmit={itemToSubmit}/>
    }else{
      renderComponent = <LoanAndInvoice itemToSubmit={itemToSubmit}/>
    }
    return (
      <FormattedWrapper locale="id" cldr={[require('../components/formatCurrency/cldr.json')]}>
        <View style={styles.container}>
          {this.state.showSignature ? (
            <SignatureView signatureView={this.signatureView.bind(this)} onSave={this._onSave.bind(this)}/>
          ):(
            this.state.showProgress ? (
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 2}}>
                <Icon name="upload-to-cloud" size={50} style={{marginBottom: 10}} />
                <Text style={{marginBottom: 10}} >{this.props.messageLoad}</Text>
                {this.props.progressBar.isShow &&
                  <ProgressBar indeterminate={true} width={200} />
                }
              </View>
            ):(
              <ScrollView>
                <List>
                  {renderComponent}
                  {this.props.imgSignature &&
                    <ListItem>
                      <View style={{flexDirection: 'column'}}>
                        <View style={styles.row}>
                          <Text style={[styles.text, styles.textField, {color: Colors.COLEKBAYAR}]}>SIGNATURE</Text>
                        </View>
                        <View style={styles.row}>
                          <Image source={{uri: this.props.imgSignature}} style={{width: 100, height: 140}} resizeMode="stretch"/>
                        </View>
                      </View>
                    </ListItem>
                  }
                  <ListItem style={{alignItems: 'center', justifyContent:'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                      <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent:'center', padding: 10}}
                        onPress={() => Actions.pop()}
                      >
                        <View>
                          <Text style={{color: 'orange', fontWeight: 'bold'}}>BATAL</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity style={{flex: 1, width: 150, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.COLEKBAYAR, padding: 10}}
                        onPress={() => this.setState({showSignature: true})}
                      >
                        <View>
                          <Text style={{color: 'white', fontWeight: 'bold'}}>CONFIRM</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </ListItem>
                </List>
              </ScrollView>
            )
          )}
        </View>
      </FormattedWrapper>
    );
  }

  signatureView(isShowSignature){
    this.setState({
      showSignature: isShowSignature
    })
  }

  _onSave(result) {
    console.log(result);
    const base64String = `data:image/jpeg;base64,${result.encoded}`;
    const encoded = `png,${result.encoded}`
    const pathImage = "file://" + result.pathName;
    var filename = pathImage.replace(/^.*[\\\/]/, '');
    let taskMedia = this.props.taskMedia;
    taskMedia.push({
      imageType: 'SGNT',
      imgUri: pathImage,
      imgBase64: encoded
    })
    this.props.setTaskMedia(taskMedia);
    let itemToSubmit = this.props.itemToSubmit;
    itemToSubmit.taskMedia = taskMedia;
    let isVisited = true;
    if(itemToSubmit.invoiceType == "detail"){
      this.props.submitTaskDetail(itemToSubmit, this.state.jwttoken, this.props.tokenId, this.props.dataUser, isVisited);
    }else{
      this.props.submitTaskPay(itemToSubmit, this.state.jwttoken, this.props.tokenId, this.props.dataUser, isVisited);
    }
    this.setState({showSignature: false, showProgress: true})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentConfirm);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10
  },
  text: {
    fontSize: 14
  },
  textField: {
    width: 150
  },
  textValue: {
    width: 200
  }
});
