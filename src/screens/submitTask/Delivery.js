/* @flow */

import React, { Component } from 'react';
import {
  View, Text,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions,
  DatePickerAndroid,
  Image,
  ActivityIndicator
} from 'react-native';
import {
  Picker, Item,
  Content,
  Radio,
} from 'native-base';
var PickerItem = Picker.Item;
import { TextInputMask } from 'react-native-masked-text';
import { FormattedCurrency } from 'react-native-globalize';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux'
import { Colors } from '../../../assets/styles/Colors'
var { height, width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialIcons'
import ImagePreview from 'react-native-image-preview';

import { openCamera, setTaskMedia } from '../../actions/taskAction'

mapStateToProps = (state) => ({
  allReason: state.taskReducer.allReason,
  paymentType: state.taskReducer.paymentType,
  bankList: state.taskReducer.bankList,
  taskMedia: state.taskReducer.taskMedia,
  dataUser: state.globalReducer.dataUser,
  isLoading: state.globalReducer.isLoading
})

mapDispatchToProps = (dispatch) => ({
  openCamera: (imageType) => {
    dispatch(openCamera(imageType));
  },
  setTaskMedia: (taskMedia) => {
    dispatch(setTaskMedia(taskMedia));
  }
})

class Delivery extends Component {
  constructor(props){
    super(props)
    this.state={
      visitList : this.props.allReason.filter(item => item.type == "VISIT_STATUS"),
      meetList : this.props.allReason.filter(item => item.type == "MEET"),
      intentionToPayList : this.props.allReason.filter(item => item.type == "Niat Bayar"),
      debtorList : this.props.allReason.filter(item => item.type == "DEBTOR"),
      debtorCoopList : this.props.allReason.filter(item => item.type == "COMPANY_CIRCUMSTANCES"),
      notPayList : this.props.allReason.filter(item => item.type == "NOT PAY"),
      partialList : this.props.allReason.filter(item => item.type == "PARTIAL"),
      selectedVisit: "", selectedMeet: "", selectedIntent: "", selectedDebtor: "", selectedDebtorCoop: "", selectedNotPay: "", selectedPartial: "",
      information: "",
      selectedCollectionStatus: "N",
      dateToPay: new Date(),
      selectedPaymentType: "",
      isOpenCamera: false,
      cashAmount: 0,
      transferAmount: 0, selectedBankTrf: "", accountOwner: "",
      chequeAmount: 0, selectedBankChq: "", cashingDate: new Date(), chequeNumber: null,
      returAmount: 0, retInvoiceNo: null, infoRetur: "",
      rawCSH: 0, rawTRF: 0, rawCHQ: 0, rawRTR: 0,
      visiblePreviewImg: false
    }
  }

  async openAndroidDatePicker() {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: this.state.dateToPay
      });
      if (action !== DatePickerAndroid.dismissedAction) {
          var date = new Date(year, month, day);
          this.setState({dateToPay: date});
      }
    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  }

  async openDatePickerCashingDate() {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: this.state.cashingDate
      });
      if (action !== DatePickerAndroid.dismissedAction) {
          var date = new Date(year, month, day);
          this.setState({cashingDate: date});
      }
    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  }

  renderSeparator(){
    return (
      <View
        style={{
          marginTop: 5,
          marginBottom: 5,
          height: 1,
          width: "100%",
          backgroundColor: Colors.GREY
        }}
      />
    )
  }

  deletePhoto(imageType){
    let tmpTaskMedia = this.props.taskMedia
    tmpTaskMedia.splice(tmpTaskMedia.map(x => x.imageType).indexOf(imageType), 1);
    this.props.setTaskMedia(tmpTaskMedia);
    Actions.submitDetail({type: 'refresh'});
  }

  render() {
    let visitList = this.state.visitList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let meetList = this.state.meetList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let intentionToPayList = this.state.intentionToPayList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let debtorList = this.state.debtorList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let debtorCoopList = this.state.debtorCoopList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let notPayList = this.state.notPayList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let bankList = this.props.bankList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let partialList = this.state.partialList.map((data, index) => {
      return <PickerItem key={index+1} value={data.id} label={data.name} />
    })
    let totalInstallment = this.state.rawCSH + this.state.rawTRF + this.state.rawCHQ + this.state.rawRTR;
    return (
      <KeyboardAvoidingView behavior="padding"  style={{flex:1}} keyboardVerticalOffset={0}>
        <View style={{padding: 10}}>
          <ScrollView>
            <View style={{flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textSeparator}>VISIT REPORTS</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.text, styles.textField, styles.textSelected]}>Visit Status</Text>
                <Picker style={{width: 175}}
                  itemTextStyle={{ fontSize: 10, color: 'white' }}
                  mode="dropdown"
                  selectedValue={this.state.selectedVisit}
                  onValueChange={(text) => this.setState({selectedVisit: text})}>
                  <PickerItem key={0} value={""} label={"Select Options"}/>
                  {visitList}
                </Picker>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.text, styles.textField, styles.textSelected]}>Meet</Text>
                <Picker style={{width: 175}}
                  mode="dropdown"
                  selectedValue={this.state.selectedMeet}
                  onValueChange={(text) => this.setState({selectedMeet: text})}>
                  <PickerItem key={0} value={""} label={"Select Options"} />
                  {meetList}
                </Picker>
              </View>
              {this.props.detailItem.invoiceType != "detail" &&
                <View style={{flexDirection: 'column'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField, styles.textSelected]}>Intention To Pay</Text>
                    <Picker style={{width: 175}}
                      mode="dropdown"
                      selectedValue={this.state.selectedIntent}
                      onValueChange={(text) => this.setState({selectedIntent: text})}>
                      <PickerItem key={0} value={""} label={"Select Options"} />
                      {intentionToPayList}
                    </Picker>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField, styles.textSelected]}>Debtor</Text>
                    <Picker style={{width: 175}}
                      mode="dropdown"
                      selectedValue={this.state.selectedDebtor}
                      onValueChange={(text) => this.setState({selectedDebtor: text})}>
                      <PickerItem key={0} value={""} label={"Select Options"} />
                      {debtorList}
                    </Picker>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField, styles.textSelected]}>Debtor Cooperative</Text>
                    <Picker style={{width: 175}}
                      mode="dropdown"
                      selectedValue={this.state.selectedDebtorCoop}
                      onValueChange={(text) => this.setState({selectedDebtorCoop: text})}>
                      <PickerItem key={0} value={""} label={"Select Options"} />
                      {debtorCoopList}
                    </Picker>
                  </View>
                </View>
              }
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.text, styles.textField]}>Information</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Content>
                  <TextInput multiline={true}
                    onChangeText={(text) => this.setState({information: text})}
                    value={this.state.information}
                    style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10, height: 60}}
                    underlineColorAndroid="white"
                  />
                </Content>
              </View>
              {this.props.detailItem.invoiceType == "detail" ? (
                <View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.textSeparator}>DELIVERY</Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 10}}>
                    <Text style={[styles.text, styles.textField]}>Recipient Name</Text>
                    <TextInput
                      onChangeText={(text) => this.setState({recipientName: text})}
                      value={this.state.recipientName}
                      style={{height: 30, padding: 0, paddingLeft: 10, fontSize: 12, borderWidth: 1, borderColor: 'gray', width: 190}}
                      underlineColorAndroid="white"
                    />
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 10}}>
                    <Text style={[styles.text, styles.textField]}>Mobile Phone Number</Text>
                    <TextInput
                      onChangeText={(text) => this.setState({phoneNo: text})}
                      value={this.state.phoneNo}
                      style={{height: 30, padding: 0, paddingLeft: 10, fontSize: 12, borderWidth: 1, borderColor: 'gray', width: 190}}
                      underlineColorAndroid="white"
                      keyboardType="phone-pad"
                    />
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Delivery Picture</Text>
                    {this.props.taskMedia.length > 0 &&
                      this.props.taskMedia.map(x => x.imageType).includes('DETS') ? (
                        <View style={{flexDirection: 'row', height: 100}}>
                          <TouchableOpacity activeOpacity={0.9} onPress={() => this.setState({visiblePreviewImg: true})}>
                            <View style={{height: 100, width:100}}>
                              <Image source={{uri: this.props.taskMedia.filter(x => x.imageType == 'DETS').map(x => x.imgUri)[0]}} 
                                style={{height: 100, width: 100, alignSelf: 'center'}}
                              />
                            </View>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => this.deletePhoto("DETS")}
                            style={{alignSelf: 'flex-end', paddingLeft: 10}}
                          >
                            <View>
                              <Icon name="delete" size={25} color='#2C77BC' />
                            </View>
                          </TouchableOpacity>
                          <ImagePreview visible={this.state.visiblePreviewImg} 
                            source={{uri: this.props.taskMedia.filter(x => x.imageType == 'DETS').map(x => x.imgUri)[0]}} 
                            close={() => this.setState({visiblePreviewImg: false})} 
                          />
                        </View>
                      ):(
                        this.props.isLoading ? (
                          <ActivityIndicator color={Colors.COLEKBAYAR} size={20}/>
                        ):(
                          <TouchableOpacity onPress={() => this.props.openCamera("DETS")}
                            style={{alignSelf: 'center', padding: 10}}
                          >
                            <View>
                              <Icon name="photo-camera" size={25} color='#2C77BC' />
                            </View>
                          </TouchableOpacity>
                        )
                      )
                    }
                  </View>
                </View>
              ):(
                <View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={[styles.text, styles.textField]}>Collection Status</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                      <Radio style={{marginRight: 10}} selected={this.state.selectedCollectionStatus == "N" ? true : false} onPress={() => this.setState({selectedCollectionStatus: "N"})}/>
                      <Text style={[styles.text, styles.textField, {marginRight: 10}]}>Not Paid</Text>
                      <Radio style={{marginRight: 10}} selected={this.state.selectedCollectionStatus == "P" ? true : false} onPress={() => this.setState({selectedCollectionStatus: "P", selectedPaymentType: 'CSH'})}/>
                      <Text style={[styles.text, styles.textField, {marginRight: 10}]}>Paid</Text>
                  </View>
                  {this.renderSeparator()}
                  {this.state.selectedCollectionStatus == "N" ? (
                    <View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.textSeparator}>NOT PAID</Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={[styles.text, styles.textField, styles.textSelected]}>Not Paid Reason</Text>
                        <Picker style={{width: 175}}
                          mode="dropdown"
                          selectedValue={this.state.selectedNotPay}
                          onValueChange={(text) => this.setState({selectedNotPay: text})}>
                          <PickerItem key={0} value={""} label={"Select Options"} />
                          {notPayList}
                        </Picker>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={[styles.text, styles.textField, styles.textSelected]}>Payment Date Appointed</Text>
                        <TouchableOpacity onPress={this.openAndroidDatePicker.bind(this)}>
                          <View>
                            <Text style={{width: 200, paddingTop: 14}}>{this.state.dateToPay.toLocaleDateString("id")}</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ):(
                    <View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.textSeparator}>PAID</Text>
                      </View>
                      <View style={{flexDirection: 'row', marginBottom: 10}}>
                        {this.props.paymentType.map((data, index) => {
                          let name;
                          if(index == 0){
                            name = "CASH"
                          }else{
                            name = data.name
                          }
                          let border={};
                          if(index == 0){
                            border={
                              borderTopLeftRadius: 17,
                              borderBottomLeftRadius: 17
                            }
                          }else if(index == 3){
                            border={
                              borderTopRightRadius: 17,
                              borderBottomRightRadius: 17
                            }
                          }
                          return(
                            <TouchableOpacity key={data.id} style={[border,{flex: 1, backgroundColor: this.state.selectedPaymentType == data.code ? Colors.ORANGE : "#ecf0f1", padding: 10, paddingTop: 15, paddingBottom: 15, alignItems: 'center', justifyContent: 'center'}]}
                              onPress={() => this.setState({selectedPaymentType: data.code})}
                            >
                              <View>
                                <Text style={{fontSize: 11, color: this.state.selectedPaymentType == data.code ? "#ecf0f1" : Colors.ORANGE, textAlign: 'center'}}>{name}</Text>
                              </View>
                            </TouchableOpacity>
                          )
                        })}
                      </View>
                      {this.state.selectedPaymentType == 'CSH' &&
                        <View>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.text, styles.textField]}>Cash Amount</Text>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Content>
                              <TextInputMask
                                ref={'cashAmount'}
                                type={'money'}
                                onChangeText={(text) => this.changeAmount(text, this.state.selectedPaymentType)}
                                options={{
                                  separator: ',',
                                  delimiter: '.',
                                  unit: 'IDR '
                                }}
                                value={this.state.cashAmount}
                                style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                              />
                            </Content>
                          </View>
                        </View>
                      }
                      {this.state.selectedPaymentType == 'TRF' &&
                        <View style={{flexDirection: 'column'}}>
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width: "75%"}}>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Transfer Amount</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Content>
                                  <TextInputMask
                                    ref={'transferAmount'}
                                    type={'money'}
                                    onChangeText={(text) => this.changeAmount(text, this.state.selectedPaymentType)}
                                    options={{
                                      separator: ',',
                                      delimiter: '.',
                                      unit: 'IDR '
                                    }}
                                    value={this.state.transferAmount}
                                    style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                                  />
                                </Content>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Bank</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Picker style={{width: 250}}
                                  mode="dropdown"
                                  selectedValue={this.state.selectedBankTrf}
                                  onValueChange={(text) => this.setState({selectedBankTrf: text})}>
                                  <PickerItem key={0} value={""} label={"Select Options"} />
                                  {bankList}
                                </Picker>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Account Owner</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Content>
                                  <TextInput
                                    onChangeText={(text) => this.setState({accountOwner: text})}
                                    value={this.state.accountOwner}
                                    style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                                    underlineColorAndroid="white"
                                    placeholder="Full Name"
                                  />
                                </Content>
                              </View>
                            </View>
                            <View style={{width: "25%"}}>
                              {this.props.taskMedia.length > 0 &&
                                this.props.taskMedia.map(x => x.imageType).includes('IMG1') ? (
                                  <View style={{alignItems: 'center', justifyContent: 'flex-start', paddingTop: 20}}>
                                    <Image source={{uri: this.props.taskMedia.filter(x => x.imageType == 'IMG1').map(x => x.imgUri)[0]}} 
                                      style={{height: 100, width: 75, alignSelf: 'flex-end'}} resizeMode="stretch"
                                    />
                                  </View>
                                ):(
                                  <TouchableOpacity style={{alignItems: 'center', justifyContent: 'flex-start', paddingTop: 20}}
                                    onPress={() => this.props.openCamera("IMG1")}
                                  >
                                    <View>
                                      <Icon name="photo-camera" size={25} color='#2C77BC' />
                                    </View>
                                  </TouchableOpacity>
                                )
                              }
                            </View>
                          </View>
                        </View>
                      }
                      {this.state.selectedPaymentType == 'CHQ' &&
                        <View style={{flexDirection: 'column'}}>
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width: "75%"}}>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Cheque Amount</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Content>
                                  <TextInputMask
                                    ref={'chequeAmount'}
                                    type={'money'}
                                    onChangeText={(text) => this.changeAmount(text, this.state.selectedPaymentType)}
                                    options={{
                                      separator: ',',
                                      delimiter: '.',
                                      unit: 'IDR '
                                    }}
                                    value={this.state.chequeAmount}
                                    style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                                  />
                                </Content>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Bank</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Picker style={{width: 250}}
                                  mode="dropdown"
                                  selectedValue={this.state.selectedBankChq}
                                  onValueChange={(text) => this.setState({selectedBankChq: text})}>
                                  <PickerItem key={0} value={""} label={"Select Options"} />
                                  {bankList}
                                </Picker>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Cashing Date</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity onPress={this.openDatePickerCashingDate.bind(this)} style={{borderWidth: 1, borderColor: 'gray', padding: 5, width: 255}}>
                                  <View>
                                    <Text style={{}}>{this.state.cashingDate.toLocaleDateString("id")}</Text>
                                  </View>
                                </TouchableOpacity>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.text, styles.textField]}>Cheque Number</Text>
                              </View>
                              <View style={{flexDirection: 'row'}}>
                                <Content>
                                  <TextInput
                                    onChangeText={(text) => this.setState({chequeNumber: text})}
                                    value={this.state.chequeNumber}
                                    style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                                    underlineColorAndroid="white"
                                    keyboardType="numeric"
                                  />
                                </Content>
                              </View>
                            </View>
                            <View style={{width: "25%"}}>
                              {this.props.taskMedia.length > 0 &&
                                this.props.taskMedia.map(x => x.imageType).includes('IMG2') ? (
                                  <View style={{alignItems: 'center', justifyContent: 'flex-start', paddingTop: 20}}>
                                    <Image source={{uri: this.props.taskMedia.filter(x => x.imageType == 'IMG2').map(x => x.imgUri)[0]}} 
                                      style={{height: 100, width: 75, alignSelf: 'flex-end'}} resizeMode="stretch"
                                    />
                                  </View>
                                ):(
                                  <TouchableOpacity style={{alignItems: 'center', justifyContent: 'flex-start', paddingTop: 20}}
                                    onPress={() => this.props.openCamera("IMG2")}
                                  >
                                    <View>
                                      <Icon name="photo-camera" size={25} color='#2C77BC' />
                                    </View>
                                  </TouchableOpacity>
                                )
                              }
                            </View>
                          </View>
                        </View>
                      }
                      {this.state.selectedPaymentType == 'RTR' &&
                        <View>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.text, styles.textField]}>Retur Amount</Text>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Content>
                              <TextInputMask
                                ref={'returAmount'}
                                type={'money'}
                                onChangeText={(text) => this.changeAmount(text, this.state.selectedPaymentType)}
                                options={{
                                  separator: ',',
                                  delimiter: '.',
                                  unit: 'IDR '
                                }}
                                value={this.state.returAmount}
                                style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                              />
                            </Content>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.text, styles.textField]}>Retur Invoice Number</Text>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Content>
                              <TextInput
                                onChangeText={(text) => this.setState({retInvoiceNo: text})}
                                value={this.state.retInvoiceNo}
                                style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                                underlineColorAndroid="white"
                                keyboardType="numeric"
                              />
                            </Content>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.text, styles.textField]}>Information</Text>
                          </View>
                          <View style={{flexDirection: 'row'}}>
                            <Content>
                              <TextInput
                                onChangeText={(text) => this.setState({infoRetur: text})}
                                value={this.state.infoRetur}
                                style={{fontSize: 12, borderWidth: 1, borderColor: 'gray', padding: 0, paddingLeft: 10}}
                                underlineColorAndroid="white"
                              />
                            </Content>
                          </View>
                        </View>
                      }
                      <View style={{flexDirection: 'row'}}>
                        <Text style={[styles.text, styles.textField, styles.textSelected]}>Total Installment</Text>
                        <FormattedCurrency
                          value={totalInstallment}
                          currency="IDR"
                          style={[styles.text, styles.textValue, {marginTop: 15}]}
                          maximumFractionDigits={2}
                        />
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={[styles.text, styles.textField, styles.textSelected]}>Total must be paid</Text>
                        <FormattedCurrency
                          value={this.props.detailItem.totalAmount}
                          currency="IDR"
                          style={[styles.text, styles.textValue, {marginTop: 15}]}
                          maximumFractionDigits={2}
                        />
                      </View>
                      {this.renderSeparator()}
                      {this.renderPartialPayment(totalInstallment, partialList)}

                    </View>
              )}
                </View>
              )}
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingBottom: 15, marginTop: 15}}>
                <TouchableOpacity style={{width: 200, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.COLEKBAYAR, padding: 12}}
                  onPress={this.submitTask.bind(this)}
                >
                  <View>
                    <Text style={{color: 'white', fontWeight: 'bold'}}>SAVE</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }

  renderPartialPayment(totalInstallment, partialList){
    if(totalInstallment < this.props.detailItem.totalAmount && totalInstallment > 0){
      return(
        <View>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.textSeparator}>PARTIAL PAYMENT</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={[styles.text, styles.textField, styles.textSelected]}>Partial Reason</Text>
            <Picker style={{width: 175}}
              mode="dropdown"
              selectedValue={this.state.selectedPartial}
              onValueChange={(text) => this.setState({selectedPartial: text})}>
              <PickerItem key={0} value={""} label={"Select Options"} />
              {partialList}
            </Picker>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={[styles.text, styles.textField, styles.textSelected]}>Payment Date Appointed</Text>
            <TouchableOpacity onPress={this.openAndroidDatePicker.bind(this)}>
              <View>
                <Text style={{width: 200, paddingTop: 14}}>{this.state.dateToPay.toLocaleDateString("id")}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }

  changeAmount(text, paymentType){
    if(paymentType == "CSH"){
      this.setState({
        cashAmount: text
      })
    }else if(paymentType == "TRF"){
      this.setState({
        transferAmount: text
      })
    }else if(paymentType == "CHQ"){
      this.setState({
        chequeAmount: text
      })
    }else if(paymentType == "RTR"){
      this.setState({
        returAmount: text
      })
    }
    this.totalInstallment(paymentType)
  }

  totalInstallment(paymentType){
    if(paymentType == 'CSH'){
      let rawCSH = this.refs['cashAmount'].getRawValue();
      this.setState({rawCSH: rawCSH})
    }else if(paymentType == 'TRF'){
      let rawTRF = this.refs['transferAmount'].getRawValue();
      this.setState({rawTRF: rawTRF})
    }else if(paymentType == 'CHQ'){
      let rawCHQ = this.refs['chequeAmount'].getRawValue();
      this.setState({rawCHQ: rawCHQ})
    }else if(paymentType == 'RTR'){
      let rawRTR = this.refs['returAmount'].getRawValue();
      this.setState({rawRTR: rawRTR})
    }
  }

  submitTask(){
    let itemToSubmit;
    var date = new Date().getDate();
    var month = new Date().getMonth();
    var year = new Date().getFullYear();
    var hour = new Date().getHours();
    var minute = new Date().getMinutes();
    var second = new Date().getSeconds();
    var trxDateMobile = date+"/"+(month+1)+"/"+year+" "+("0" + hour).slice(-2)+":"+("0" + minute).slice(-2)+":"+("0" + second).slice(-2);
    if(this.props.detailItem.invoiceType == "detail"){
      itemToSubmit = {
        username: this.props.dataUser.userName,
        invoiceNumber : this.props.detailItem.invoiceNo,
        trxDateMobile: trxDateMobile,
        distributionId : this.props.detailItem.id,
        activityId: this.props.detailItem.aktivitas[0].activityId,
        reasonStatus: this.state.selectedVisit,
        reasonVisit: this.state.selectedMeet,
        information : this.state.information,
        invoiceType: this.props.detailItem.invoiceType,
        outletName : this.props.detailItem.outletName,
        customerName : this.props.detailItem.customerName,
        namaPenerima: this.state.recipientName,
        NoTelp: this.state.phoneNo,
        taskMedia: this.props.taskMedia
      }
    }else{
      let paymentType = [];
      if(this.state.rawCSH != 0){
        paymentType.push({
          p: "CSH",
          a: this.state.rawCSH
        })
      }
      if(this.state.rawCHQ != 0){
        paymentType.push({
          p: 'CHQ',
          a: this.state.rawCHQ,
          b: this.state.selectedBankChq,
          d: this.state.cashingDate.toLocaleDateString("id"),
          r: this.state.chequeNumber
        })
      }
      if(this.state.rawTRF != 0){
        paymentType.push({
          p: 'TRF',
          a: this.state.rawTRF,
          b: this.state.selectedBankTrf,
          r: this.state.accountOwner
        })
      }
      if(this.state.rawRTR != 0){
        paymentType.push({
          p: 'RTR',
          a: this.state.rawRTR,
          r: this.state.retInvoiceNo
        })
      }
      itemToSubmit = {
        username: this.props.dataUser.userName,
        invoiceNumber : this.props.detailItem.invoiceNo,
        installment : this.state.rawCSH + this.state.rawTRF + this.state.rawCHQ + this.state.rawRTR,
        trxDateMobile: trxDateMobile,
        distributionId : this.props.detailItem.id,
        activityId: this.props.detailItem.aktivitas[0].activityId,
        reasonStatus: this.state.selectedVisit,
        reasonVisit: this.state.selectedMeet,
        reasonAttitude : this.state.selectedIntent,
        reasonDebitur : this.state.selectedDebtor,
        reasonIsCooperative : this.state.selectedDebtorCoop,
        information : this.state.information,
        isPaid: this.state.selectedCollectionStatus,
        paymentType: paymentType,
        isPartial: {
          reason: this.state.selectedPartial,
          nextCollectionDate: this.state.dateToPay.toLocaleDateString("id"),
          information : this.state.information,
        },
        isNotPaid: {
          reason: this.state.selectedNotPay,
          nextCollectionDate: this.state.dateToPay.toLocaleDateString("id"),
          information : this.state.information,
        },
        invoiceType: this.props.detailItem.invoiceType,
        invoiceAmount: this.props.detailItem.totalAmount,
        outletName : this.props.detailItem.outletName,
        customerName : this.props.detailItem.customerName,
        taskMedia: this.props.taskMedia
      }
    }
    Actions.paymentConfirm({itemToSubmit})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Delivery);

const styles = StyleSheet.create({
  textSeparator: {
    fontSize: 15,
    color: Colors.COLEKBAYAR,
    fontWeight: 'bold',
    paddingBottom: 10,
    paddingTop: 10
  },
  text: {
    fontSize: 14
  },
  textField: {
    width: 150
  },
  textValue: {
    width: 200
  },
  textSelected: {
    paddingTop: 14
  },
  preview: {
    flex: 1,
    height: height,
    zIndex:1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
});
