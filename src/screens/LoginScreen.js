/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  AsyncStorage,
  PermissionsAndroid
} from 'react-native';
import { connect } from 'react-redux'
import {Actions} from 'react-native-router-flux'

import LoginMobilePhone from './login/MobilePhone'
import LoginOTP from './login/OTP'
import CreatePin from './login/CreatePin'
import LoginPin from './login/LoginPin'

mapStateToProps = (state) => state;

mapDispatchToProps = (dispatch) => ({

})

class LoginScreen extends Component {
  componentWillMount(){
    AsyncStorage.getItem("storagePin", (error, result) => {
      if(result == null || result == undefined){
        Actions.refresh({statusLogin: 'mobilePhone'})
      }else{
        Actions.refresh({statusLogin: 'loginPin'})
      }
    })
  }

  render() {
    let _renderComponent;
    if(this.props.statusLogin == "mobilePhone"){
      _renderComponent = <LoginMobilePhone/>;
    }else if(this.props.statusLogin == "otp"){
      _renderComponent = <LoginOTP/>;
    }else if(this.props.statusLogin == "createPin"){
      _renderComponent = <CreatePin/>;
    }else if(this.props.statusLogin == "loginPin"){
      _renderComponent = <LoginPin/>;
    }
    return (
      <View style={styles.container}>
        <View style={styles.imageView}>
          <Image source={require('../../assets/images/colekbayar-logo.png')} style={styles.imageLogo}/>
        </View>
        {_renderComponent}
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  imageLogo: {
    margin: 35,
    width: 150
  },
  imageView: {
    alignItems: 'center',
    paddingTop: 65
  },
});
