const BASE_URL = 'https://aws.colekbayar.com/api';

const API = {
  loginApi(imsi, imei, deviceId, phoneNo){
    const data = new FormData();
    data.append("imsi", imsi);
    data.append("imei", imei);
    data.append("deviceid", deviceId);
    data.append("phoneNo", phoneNo);
    let config = {
      method: 'POST',
      body: data,
      credentials: 'same-origin'
    }
    return fetch(`${BASE_URL}/login`, config)
  },
  activate(phoneNo, tokenotp, jwttoken){
    const data = new FormData();
    data.append("phoneNo", phoneNo);
    data.append("tokenotp", tokenotp);
    data.append("jwttoken", jwttoken);

    const headers = new Headers();
    headers.append('jwttoken', jwttoken);

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }
    return fetch(`${BASE_URL}/activatemobile`, config);
  },
  taskList(jwttoken, tokenId){
    const data = new FormData();
    data.append("jwttoken", jwttoken);
    data.append("tokenId", tokenId);

    const headers = new Headers();
    headers.append('jwttoken', jwttoken);

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }
    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/taskList`, config)
  },
  uploadImage(config){
    return fetch(`${BASE_URL}/zuultest/mobile/api/v1/mobile/uploadImage`, config)
  },
  submitPaymentTask(config){
    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/pay`, config)
  },
  submitDetailTask(config){
    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/delivery`, config)
  },
  saveTracking(config){
    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/saveTracking`, config)
  },
  settleList(jwttoken, tokenId){
    const data = new FormData();
    data.append("jwttoken", jwttoken);
    data.append("tokenId", tokenId);

    const headers = new Headers()
    headers.append("jwttoken", jwttoken);

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }

    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/settleList`, config)
  },
  submitSettle(jwttoken, tokenId, refNo){
    const data = new FormData()
    data.append("jwttoken", jwttoken)
    data.append("tokenId", tokenId)
    data.append("refNo", refNo)

    const headers = new Headers()
    headers.append("jwttoken", jwttoken)

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }

    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/settlement`, config)
  },
  activityList(jwttoken, tokenId){
    const data = new FormData()
    data.append("jwttoken", jwttoken)
    data.append("tokenId", tokenId)

    const headers = new Headers()
    headers.append("jwttoken", jwttoken)

    let config = {
      method: 'POST',
      body: data,
      headers: headers,
      credentials: 'same-origin'
    }

    return fetch(`${BASE_URL}/zuul/mobile/api/v1/mobile/activityList`, config)
  }
}

export default API;
