/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import { Colors } from '../../assets/styles/Colors'
import Location from './Location'

export default class HeaderImage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../assets/images/icon.png')} style={{width: 75, height: 30}} resizeMode={'stretch'}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.COLEKBAYAR,
    padding: 3,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50
  },
});
