/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import { Colors } from '../../assets/styles/Colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Actions } from 'react-native-router-flux'

var { height, width } = Dimensions.get('window')

export default class NavBarCustom extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{width: '10%', alignItems: 'center'}}>
          <TouchableHighlight onPress={() => Actions.pop()}>
            <View>
              <Icon name="arrow-back" size={25} color='#fff'/>
            </View>
          </TouchableHighlight>
        </View>
        <View style={{width: '90%', alignItems: 'center', marginLeft: -15}}>
          <Text style={{textAlign: 'center', color: 'white', fontSize: 16, fontWeight: 'bold'}}>{this.props.title.toUpperCase()}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.COLEKBAYAR,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    flexDirection: 'row'
  },
});
