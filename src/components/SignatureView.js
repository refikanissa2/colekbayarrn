/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux'
import SignatureCapture from 'react-native-signature-capture'
var { height, width } = Dimensions.get('window')

mapStateToProps = (state) => ({

})

mapDispatchToProps = (dispatch) => ({

})

class SignatureView extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentRow}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{fontSize: 14}}>Please write your signature.</Text>
          </View>
          <TouchableOpacity onPress={() => this.props.signatureView(false)}>
            <View>
              <Text>{' x '}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <SignatureCapture
          style={styles.signature}
          onDragEvent={this._onDragEvent.bind(this)}
          onSaveEvent={this._onSaveEvent.bind(this)}
          showTitleLabel={true}
          saveImageFileInExtStorage={true}
          viewMode={"portrait"}
        />
      </View>
    );
  }
  _onDragEvent() {
  }

  _onSaveEvent(result) {
    this.props.onSave && this.props.onSave(result);
}
}

export default connect(mapStateToProps, mapDispatchToProps)(SignatureView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10
  },
  contentRow: {
    flexDirection: 'row'
  },
  signature: {
    flex: 1,
    // borderColor: '#000033',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  buttonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    backgroundColor: "#eeeeee",
    margin: 10
  },
});
