/* @flow */

import React, { Component } from 'react';
import {
  View, AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { saveTracking, setPosition } from '../actions/globalAction'

mapStateToProps = (state) => ({
  dataPosition: state.globalReducer.dataPosition,
  tokenId: state.globalReducer.tokenId,
  dataUser: state.globalReducer.dataUser
})

mapDispatchToProps = (dispatch) => ({
  setPosition: (dataPosition) => {
    dispatch(setPosition(dataPosition));
  },
  saveTracking: (jwttoken, tokenId, dataPosition, nik) => {
    dispatch(saveTracking(jwttoken, tokenId, dataPosition, nik));
  }
})

class Location extends Component {
  componentDidMount(){
    let jwttoken;
    AsyncStorage.getItem("jwttoken", (err, result) => {
      jwttoken = result;
    })
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        var tmpPosition = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          timeTracking: new Date().toLocaleString("id")
        }
        var dataPosition = this.props.dataPosition;
        dataPosition.push(tmpPosition);
        this.props.setPosition(dataPosition);
        if(dataPosition.length == 10){
          this.props.saveTracking(jwttoken, this.props.tokenId, dataPosition, this.props.dataUser.userName);
        }
      },
      (error) => console.log("error ", error),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 }
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  render() {
    return (
      <View/>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Location);
