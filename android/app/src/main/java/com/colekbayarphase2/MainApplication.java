package com.colekbayarphase2;

import android.app.Application;

import com.convertbase64.ImageToBase64Package;
import com.facebook.react.ReactApplication;
import com.imagepicker.ImagePickerPackage;
import com.jimmydaddy.imagemarker.ImageMarkerPackage;
import com.rssignaturecapture.RSSignatureCapturePackage;
import com.imeigetter.RNIMEIPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new ImagePickerPackage(),
            new ImageMarkerPackage(),
            new RSSignatureCapturePackage(),
            new RNDeviceInfo(),
            new RNIMEIPackage(),
              new ImageToBase64Package()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
