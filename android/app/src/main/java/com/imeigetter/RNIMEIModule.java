package com.imeigetter;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RNIMEIModule extends ReactContextBaseJavaModule {
    ReactApplicationContext reactContext;

    public RNIMEIModule(ReactApplicationContext reactContext){
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName(){ return "IMEI"; }

    @ReactMethod
    public void getIMEI(Promise promise){
        try{
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService(Context.TELEPHONY_SERVICE);
            promise.resolve(tm.getDeviceId());
        } catch (Exception e){
            promise.reject(e.getMessage());
        }
    }

    @ReactMethod
    public void getIMSI(Promise promise){
        try{
            TelephonyManager tm = (TelephonyManager) this.reactContext.getSystemService((Context.TELEPHONY_SERVICE));
            promise.resolve(tm.getSubscriberId());
        } catch (Exception e){
            promise.reject(e.getMessage());
        }
    }

    @ReactMethod
    public void md5(String s, Promise promise) throws NoSuchAlgorithmException {
        try {
            if(s.isEmpty()){
                String result = "";
                promise.resolve(result);
            } else {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                digest.update(s.getBytes());
                byte[] messageDigest = digest.digest();
                StringBuilder hexString = new StringBuilder();
                byte[] var5 = messageDigest;
                int var6 = messageDigest.length;

                for(int var7 = 0; var7 < var6; ++var7){
                    byte aMessageDigest = var5[var7];
                    hexString.append(Integer.toHexString(255 & aMessageDigest));
                }
                promise.resolve(hexString.toString());
            }
        } catch (Exception e){
            promise.reject(e.getMessage());
        }
    }

    @ReactMethod
    public void genTokenKey(String s, Promise promise) throws NoSuchAlgorithmException {
        try {
            if(s.isEmpty()){
                String result = "";
                promise.resolve(result);
            } else {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                digest.update(s.getBytes());
                byte[] messageDigest = digest.digest();
                StringBuilder hexString = new StringBuilder();
                byte[] var5 = messageDigest;
                int var6 = messageDigest.length;

                for(int var7 = 0; var7 < var6; ++var7){
                    byte aMessageDigest = var5[var7];
                    hexString.append(Integer.toHexString(0xFF & aMessageDigest));
                }
                promise.resolve(hexString.toString());
            }
        } catch (Exception e){
            promise.reject(e.getMessage());
        }
    }
}
