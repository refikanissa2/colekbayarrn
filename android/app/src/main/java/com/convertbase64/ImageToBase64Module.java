package com.convertbase64;

import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by refikanissa on 1/26/2018.
 */

public class ImageToBase64Module extends ReactContextBaseJavaModule {
    ReactApplicationContext reactContext;

    public ImageToBase64Module(ReactApplicationContext reactContext){
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNImageToBase64";
    }

    @ReactMethod
    public void getBase64String(String uri, Callback callback) {
        try {
            Bitmap image = MediaStore.Images.Media.getBitmap(this.reactContext.getContentResolver(), Uri.parse(uri));
            if (image == null) {
                callback.invoke("Failed to decode Bitmap, uri: " + uri);
            } else {
                callback.invoke(null, bitmapToBase64(image));
            }
        } catch (IOException e) {
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }
}
